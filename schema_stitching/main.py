from graphql_api import field, GraphQLAPI
from graphql_api.context import GraphQLContext
from graphql_api.remote import GraphQLRemoteExecutor, remote_execute

from graphql_http_server import GraphQLHTTPServer

PokemonAPI = GraphQLRemoteExecutor(
    name="PokemonAPI",
    url="https://graphql-pokemon2.vercel.app/",
    http_method="POST"
)

RickAndMortyAPI = GraphQLRemoteExecutor(
    name="RickAndMortyAPI",
    url="https://rickandmortyapi.com/graphql",
    http_method="POST"
)


class SchemaStitchingDemo:

    @field
    def pokemon_api(self, context: GraphQLContext) -> PokemonAPI:
        return remote_execute(executor=PokemonAPI, context=context)

    @field
    def rick_and_morty_api(self, context: GraphQLContext) -> RickAndMortyAPI:
        return remote_execute(executor=RickAndMortyAPI, context=context)


default_query = """# Schema stitching example written in Python with ObjectQL

query {
    pokemonApi {
        pokemon(name: "Pikachu") {
            types
        }
    }
    rickAndMortyApi {
        character(id: 1) {
            name
        }
    }
}
"""

server = GraphQLHTTPServer.from_api(
    api=GraphQLAPI(root=SchemaStitchingDemo),
    graphiql_default_query=default_query
)


# define main function for Google Cloud Function support
def main(request):
    return server.dispatch(request=request)

if __name__ == "__main__":
    server.run(main=main, port=3508)
