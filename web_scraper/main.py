from typing import List

from pyquery import PyQuery

from graphql_api import GraphQLAPI, field, type
from graphql_http_server import GraphQLHTTPServer


@type(interface=True)
class Node:
    """
    A Node represents a DOM Node
    """

    def __init__(self, pq):
        self.pq: PyQuery = pq

    def find(self, selector: str = None) -> PyQuery:
        return self.pq.find(selector) if selector is not None else self.pq

    @field
    def content(self, selector: str = None) -> str:
        """
        The html representation of the subnodes for the selected DOM
        """
        return self.find(selector).eq(0).html()

    @field
    def html(self, selector: str = None) -> str:
        """
        The html representation of the selected DOM
        """
        return self.find(selector).outerHtml()

    @field
    def text(self, selector: str = None) -> str:
        """
        The text for the selected DOM
        """
        return self.find(selector).eq(0).remove('script').text()

    @field
    def tag(self, selector: str) -> str:
        """
        The tag for the selected DOM
        """
        el = self.find(selector).eq(0)
        if el:
            return el[0].tag

    @field
    def attr(self, name: str, selector: str = None) -> str:
        """
        The DOM attr of the Node
        """
        return self.find(selector).attr(name)

    @field
    def _is(self, selector: str = None) -> bool:
        """
        Returns True if the DOM matches the selector
        """
        return self.pq.is_(selector)

    @field
    def _query(self, selector: str) -> List['Element']:
        """
        Find elements using selector traversing down from self
        """
        return list(map(Element, self.find(selector).items()))

    @field
    def children(self, selector: str = None) -> List['Element']:
        """
        The list of children elements from self
        """
        return list(map(Element, self.pq.children(selector).items()))

    @field
    def parents(self, selector: str = None) -> List['Element']:
        """
        The list of parent elements from self
        """
        return list(map(Element, self.pq.parents(selector).items()))

    @field
    def parent(self) -> 'Element':
        """
        The parent element from self
        """
        parent = self.pq.parents().eq(-1)
        if parent:
            return Element(parent)

    @field
    def siblings(self, selector: str = None) -> List['Element']:
        """
        The siblings elements from self
        """
        return list(map(Element, self.pq.siblings(selector).items()))

    @field
    def next(self, selector: str = None) -> 'Element':
        """
        The immediately following sibling from self
        """
        _next = self.pq.next_all(selector)
        if _next:
            return Element(_next.eq(0))

    @field
    def next_all(self, selector: str = None) -> List['Element']:
        """
        The list of following siblings from self
        """
        return list(map(Element, self.pq.next_all(selector).items()))

    @field
    def prev(self, selector: str = None) -> 'Element':
        """
        The immediately preceding sibling from self
        """
        _prev = self.pq.prev_all(selector)
        if _prev:
            return Element(_prev.eq(0))

    @field
    def prev_all(self, selector: str = None) -> List['Element']:
        """
        The list of preceding siblings from self
        """
        return list(map(Element, self.pq.prevAll(selector).items()))


class Document(Node):
    """
    The Document Type represent any web page loaded and
    serves as an entry point into the page content
    """

    def __init__(self, url=None, source=None):
        if not url and not source:
            raise KeyError("Document requires a URL or a Source")

        super().__init__(pq=PyQuery(source) if source else PyQuery(url=url))

    @field
    def title(self) -> str:
        """
        The title of the document
        """
        return self.find('title').eq(0).text()


class Element(Node):
    """
    A Element Type represents an object in a Document
    """

    @field
    def visit(self) -> Document:
        """
        Visit will visit the href of the link and return the
        corresponding document
        """
        if self._is('a'):
            return Document(self.attr(name='href'))

        raise TypeError(f"Element '{self}' is not a link.")


class Query:
    """
    Web scraper written in Python with GraphQL-API built using `PyQuery` and influenced by [graphql-scraper](https://github.com/lachenmayer/graphql-scraper) and [gdom](https://github.com/syrusakbary/gdom). The Web Scraper example is API compatible with `graphql-scraper` and `gdom`.
    """

    @field
    def page(self, url: str = None, source: str = None) -> Document:
        """
        Visit the specified page
        """
        return Document(url=url, source=source)


default_query = """# Web scraper written in Python with GraphQL-API built using `PyQuery` and influenced by [graphql-scraper](https://github.com/lachenmayer/graphql-scraper) and [gdom](https://github.com/syrusakbary/gdom). The Web Scraper example is API compatible with `graphql-scraper` and `gdom`.

{
  page(url:"http://news.ycombinator.com") {
    items: query(selector:"tr.athing") {
      rank: text(selector:"td span.rank")
      title: text(selector:"td.title a")
      sitebit: text(selector:"span.comhead a")
      url: attr(selector:"td.title a", name:"href")
      attrs: next {
         score: text(selector:"span.score")
         user: text(selector:"a:eq(0)")
         comments: text(selector:"a:eq(2)")
      }
    }
  }
}
"""

server = GraphQLHTTPServer.from_api(
    api=GraphQLAPI(root=Query),
    graphiql_default_query=default_query
)


# define main function for Google Cloud Function support
def main(request):
    return server.dispatch(request=request)


if __name__ == "__main__":
    server.run(main=main, port=3508)
