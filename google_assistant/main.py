import os
import json

import google.auth.transport.grpc
import google.auth.transport.requests
import google.oauth2.credentials

from google.assistant.embedded.v1alpha2 import (
    embedded_assistant_pb2,
    embedded_assistant_pb2_grpc
)

from graphql_api import GraphQLAPI, field
from graphql_http_server import GraphQLHTTPServer


class Assistant:

    def __init__(self):
        # Load OAuth 2.0 credentials.
        credentials_path = os.path.join(os.path.dirname(__file__), "credentials.json")
        with open(credentials_path, 'r') as f:
            credentials = google.oauth2.credentials.Credentials(
                token=None,
                **json.load(f)
            )
            http_request = google.auth.transport.requests.Request()
            credentials.refresh(http_request)

        # Create an authorized gRPC channel.
        self.assistant = embedded_assistant_pb2_grpc.EmbeddedAssistantStub(
            google.auth.transport.grpc.secure_authorized_channel(
                credentials, http_request, 'embeddedassistant.googleapis.com'
            )
        )

        self.config = embedded_assistant_pb2.AssistConfig(
            audio_out_config=embedded_assistant_pb2.AudioOutConfig(
                encoding='LINEAR16',
                sample_rate_hertz=16000,
                volume_percentage=0,
            ),
            dialog_state_in=embedded_assistant_pb2.DialogStateIn(
                language_code="en-GB",
                conversation_state=None,
                is_new_conversation=True,
            ),
            device_config=embedded_assistant_pb2.DeviceConfig(
                device_id="1234",
                device_model_id="1234",
            )
        )

    @field
    def assist(self, query: str) -> str:

        def iter_assist_requests():
            self.config.text_query = query
            yield embedded_assistant_pb2.AssistRequest(config=self.config)

        response = self.assistant.Assist(iter_assist_requests(), 60 * 3 + 5)

        for resp in response:
            if resp.dialog_state_out.supplemental_display_text:
                return resp.dialog_state_out.supplemental_display_text or ""

        return ""


path = os.path.join(os.path.dirname(__file__), './example.graphql')
with open(path, mode='r') as file:
    default_query = file.read()

server = GraphQLHTTPServer.from_api(
    api=GraphQLAPI(root=Assistant),
    graphiql_default_query=default_query
)


# define main function for Google Cloud Function support
def main(request):
    return server.dispatch(request=request)


# define app for Dockerfile/gunicorn support
app = server.app(main=main)

if __name__ == "__main__":
    server.run(main=main, port=3502)
