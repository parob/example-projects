import os

from dataclasses import dataclass
from typing import List
from uuid import UUID

from graphql_api import field, GraphQLAPI
from sqlalchemy_gql.orm_base import DatabaseManager, ModelBase
from sqlalchemy_gql.relay_base import relay_connection

from sqlalchemy_orm.filter import Filter
from sqlalchemy_orm.order_by import OrderBy

from graphql_http_server import GraphQLHTTPServer


@dataclass
class Note(ModelBase):
    title: str = ""
    note: str = ""

    @field(mutable=True)
    def update(self, title: str = None, note: str = None) -> 'Note':
        if title is not None:
            self.title = title

        if note is not None:
            self.note = note

        return self


@dataclass
class NoteFilter(Filter):
    ids: List[UUID] = None
    title: str = ""

    def criterion(self, entities=None):
        criterion = super().criterion(entities=entities)

        if self.ids:
            criterion.append(Note.id.in_(self.ids))

        if self.title:
            criterion.append(Note.title.ilike('%' + self.title + '%'))

        return criterion


RelayConnection = relay_connection(Note)


class Notes:

    @field(mutable=True)
    def create_note(self, title: str, note: str) -> Note:
        note = Note(title=title, note=note)
        note.create()
        return note

    @field
    def all_notes(
        self,
        order_by: List[OrderBy] = None,
        filter: NoteFilter = None,
        before: str = None,
        after: str = None,
        first: int = None,
        last: int = None
    ) -> RelayConnection:
        """
        Find `Notes`

        Returns a Relay Connection
        """

        return RelayConnection(
            Note,
            order_by=order_by,
            filter=filter,
            before=before,
            after=after,
            first=first,
            last=last
        )

    @field
    def note(self, id: UUID = None, title: str = None) -> Note:
        if id:
            return Note.query().filter(Note.id == id).one()
        if title:
            return Note.query().filter(Note.title == title).one()


# Check if we are deployed in Google Cloud Functions
is_google_cloud_function = os.getenv("FUNCTION_SIGNATURE_TYPE", os.getenv("FUNCTION_TRIGGER_TYPE")) == "http"

if is_google_cloud_function:     # Use an in-memory database in Google Cloud Functions
    db_manager = DatabaseManager(url="sqlite:///:memory:")
else:
    db_manager = DatabaseManager()
    db_manager.install()


path = os.path.join(os.path.dirname(__file__), './example.graphql')
with open(path, mode='r') as file:
    default_query = file.read()

server = GraphQLHTTPServer.from_api(
    api=GraphQLAPI(root=Notes),
    graphiql_default_query=default_query
)


@db_manager.with_db_session
def main(request):

    # Reinstall the models if in Google Cloud Functions and DB is empty
    if is_google_cloud_function and db_manager.db.is_empty():
        db_manager.install()
        
    return server.dispatch(request=request)


if __name__ == "__main__":
    server.run(main=main, port=3506)
