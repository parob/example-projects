import threading

import pytest

from app_pool import adapter as pool_adapter, db_manager

from app_worker import adapter as worker_adapter


@pytest.fixture()
def db_session():
    return db_manager.db.session


@pytest.fixture(scope="session")
def job_pool():
    pool = pool_adapter.root_value

    pool_thread = threading.Thread(
        target=lambda: pool_adapter.run_app(
            port=5656,
            main=db_manager.with_db_session(pool_adapter.dispatch)
        )
    )
    pool_thread.setDaemon(True)
    pool_thread.start()

    return pool


@pytest.fixture(scope="session")
def worker():
    worker = worker_adapter.root_value
    worker.job_pool_url = "http://localhost:5656"
    worker.start()

    worker_thread = threading.Thread(
        target=lambda: worker_adapter.run_app(port=5657)
    )
    worker_thread.setDaemon(True)
    worker_thread.start()

    return worker
