import os

from dispatchers import Shell
from job import JobResult


class TestShellHandler:

    def test_echo(self):

        cli_executor = Shell(input_data="echo 'hello'")

        cli_executor.start()
        cli_executor.join()

        assert JobResult.success == cli_executor.result
        assert 'hello' == cli_executor.output_data

    def test_cdir(self):
        test_dir_name = os.path.join(os.path.dirname(__file__), "test_cli_dir")

        assert not os.path.exists(test_dir_name)

        cli_executor = Shell(input_data=f"mkdir '{test_dir_name}'")

        cli_executor.start()
        cli_executor.join()

        assert JobResult.success == cli_executor.result
        assert os.path.exists(test_dir_name)

        os.rmdir(test_dir_name)

    def test_escape_dir(self):
        cli_executor = Shell(input_data="cd ..\nmkdir test")

        cli_executor.start()
        cli_executor.join()

        assert JobResult.failure == cli_executor.result
