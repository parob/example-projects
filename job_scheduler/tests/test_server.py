import time

from context_helper import Context

from job import Job, JobState
from pool import JobPool
from worker import Worker


# noinspection DuplicatedCode
class TestServer:

    def test_found_worker(self, job_pool: JobPool, worker: Worker):
        timeout = 10
        interval = 0.1
        count = 0

        while not job_pool.worker_info() and count < (timeout/interval):
            time.sleep(interval)
            count += 1

        assert worker.active()
        assert len(job_pool.worker_info()) == 1

    def test_create_job(self, job_pool: JobPool, worker: Worker, db_session):
        timeout = 10
        interval = 0.1
        count = 0
        session = db_session()

        with Context(db_session=session):
            id = job_pool.create_job(job=Job(type="shell", input_data="ls")).id

        session.commit()

        job = None

        while (job is None or job.state != JobState.completed) \
            and count < (timeout/interval):

            with Context(db_session=db_session()):
                job = job_pool.job(id=id)
                time.sleep(interval)
                count += 1

        assert worker.active()
        assert job.state == JobState.completed

    def test_create_job_chain(
        self, 
        job_pool: JobPool, 
        worker: Worker, 
        db_session
    ):
        timeout = 20
        interval = 0.1
        count = 0
        session = db_session()

        sleep_job_duration = 3

        job_chain = [
            Job(type="shell", input_data="ls"),
            Job(type="sleep", input_data=str(sleep_job_duration)),
            Job(type="shell", input_data="pwd")
        ]

        with Context(db_session=session):
            job_chain = job_pool.create_job_chain(jobs=job_chain)

        session.commit()

        last_job_id = job_chain[-1].id

        last_job = None

        while (last_job is None or last_job.state != JobState.completed) \
            and count < (timeout/interval):

            with Context(db_session=db_session()):
                last_job = job_pool.job(id=last_job_id)
                time.sleep(interval)
                count += 1

        assert worker.active()
        assert last_job.state == JobState.completed
        assert interval * count > sleep_job_duration

chain_query = """
    mutation {
      createJobChain(jobs: [
        {type:"shell", inputData:"ls"},
        {type:"shell", inputData:"ls"},
        {type:"shell", inputData:"ls"},
        {type:"shell", inputData:"ls"},
        {type:"sleep", inputData:"5"},
        {type:"docker_build", inputData:""},
        {type:"sleep", inputData:"5"}

      ]) {
        id
        state
      }
    }
"""
