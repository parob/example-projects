import pytest

from dispatchers import DockerBuild
from job import JobResult


class TestDockerBuildHandler:

    @pytest.mark.skipif(
        not DockerBuild.available(), reason="requires docker to be installed and running"
    )
    def test_build(self):
        build_executor = DockerBuild(input_data="{}")

        build_executor.start()
        build_executor.join()

        assert JobResult.success == build_executor.result
