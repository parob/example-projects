# define main function for Google Cloud Function support
from job_scheduler.app_pool import app, main  # noqa: F401

if __name__ == "__main__":
    import asyncio

    from hypercorn.config import Config
    from hypercorn.asyncio import serve

    config = Config()
    config.bind = ["localhost:8080"]
    asyncio.run(serve(app, config))