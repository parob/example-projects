import subprocess

from job_scheduler.dispatchers.shell import Shell


class DockerBuild(Shell):

    __type__ = "docker_build"

    @classmethod
    def available(cls):
        if not super().available():
            return False

        # noinspection PyBroadException
        try:
            if subprocess.call('docker ps', shell=True) != 0:
                return False

            if subprocess.call('git --version', shell=True) != 0:
                return False

        except Exception:
            return False

        return True

    def __init__(self, *arg, **kwargs):
        super().__init__(*arg, **kwargs)

        # self.data = json.loads(input_data)

        test_repo = "https://github.com/shekhargulati/" \
                    "python-flask-docker-hello-world.git"

        self.commands = [
            f"git clone {test_repo}",
            "cd python-flask-docker-hello-world",
            "docker build -t python-flask-docker-hello-world ."
        ]
