import os
import shlex
import shutil
import subprocess
import uuid

from job_scheduler.dispatchers.base import Dispatcher
from job_scheduler.job import JobResult


class Shell(Dispatcher):

    __type__ = "shell"

    def __init__(self, *arg, **kwargs):
        super().__init__(*arg, **kwargs)
        self.shell = None

        self.dir_id = str(uuid.uuid4())
        self.cwd = os.getcwd()
        self.dir_path = os.path.join(self.cwd, self.dir_id)
        self.commands = []

        for line in self.input_data.splitlines():
            self.commands += line.rstrip('\n').split(";")

    def run(self):
        os.mkdir(self.dir_path)
        os.chdir(self.dir_path)

        output_data = ""

        def cleanup():
            shutil.rmtree(self.dir_path)
            os.chdir(self.cwd)

        try:
            for cmd in self.commands:
                cmd_list = shlex.split(cmd)
                command = cmd_list[0]
                args = cmd_list[1:]

                self.logs.append(">>> " + cmd)

                if command == "cd":
                    os.chdir(args[0])
                    if not os.getcwd().startswith(self.dir_path):
                        self.result = JobResult.failure
                        self.logs.append(
                            "PermissionError: Attempted to `cd` "
                            "outside of the home directory."
                        )
                        return

                else:
                    process = subprocess.Popen(
                        [command] + args,
                        stdout=subprocess.PIPE,
                        stderr=subprocess.STDOUT
                    )

                    while True:
                        stdout = process.stdout.readlines()
                        if stdout == [] and process.poll() is not None:
                            break

                        if stdout:
                            lines = [
                                line.decode("utf-8").strip()
                                for line in stdout
                            ]
                            output_data += "\n".join(lines)
                            self.logs.extend(lines)

                    return_code = process.poll()

                    if return_code != 0:
                        self.logs.append(f'Return code {return_code}')
                        self.result = JobResult.failure
                        return

                self.result = JobResult.success

        except Exception as err:
            print(f"error {err}")
            self.logs += [f'Exception error {err}']
            self.result = JobResult.failure

        finally:
            cleanup()
            self.output_data = output_data
