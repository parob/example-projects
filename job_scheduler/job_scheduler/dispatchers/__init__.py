# flake8: noqa

from job_scheduler.dispatchers.base import Dispatcher
from job_scheduler.utils import get_all_subclasses

from job_scheduler.dispatchers.sleep import Sleep
from job_scheduler.dispatchers.shell import Shell
from job_scheduler.dispatchers.docker_build import DockerBuild

all_dispatchers = get_all_subclasses(Dispatcher)
