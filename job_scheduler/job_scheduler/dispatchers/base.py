from multiprocessing import Process, Manager
from typing import List, Callable

from job_scheduler.job import JobResult, JobState, Job


class Dispatcher(Process):

    __type__ = "base"

    @classmethod
    def available(cls) -> bool:
        """
        Returns true if the system requirements for this handler are met
        :return:
        """
        return True

    @classmethod
    def dispatch(
        cls,
        job: Job,
        timeout: int,
        on_complete: Callable[[JobResult], None]
    ):
        log_index = 0
        manager = Manager()

        job_data = manager.Namespace()
        logs: List[str] = manager.list()

        dispatch = cls(input_data=job.input_data, job_data=job_data, logs=logs)
        # dispatch.daemon = True

        job.progress(state=JobState.running)
        dispatch.start()

        total_time = 0
        update_log_poll_interval = 1

        while total_time < timeout and dispatch.is_alive():
            dispatch.join(update_log_poll_interval)
            total_time += update_log_poll_interval
            new_logs = logs[log_index:]
            log_index = len(logs)
            job.update_logs(logs=new_logs)

        if dispatch.is_alive():
            dispatch.terminate()
            dispatch.result = JobResult.timeout
            if timeout < job.timeout:
                dispatch.logs += [
                    f"Job timed out, Worker timeout {timeout}s limit reached."
                ]
            else:
                dispatch.logs += [
                    f"Job timed out, Job timeout {job.timeout}s limit reached."
                ]

        job.result = dispatch.result
        job.complete(
            result=dispatch.result,
            output_data=dispatch.output_data
        )

        on_complete(job)

    def __init__(self, input_data: str, job_data=None, logs: List=None):
        super().__init__()
        manager = Manager()

        if not job_data:
            job_data = manager.Namespace()

        if not logs:
            logs = manager.list()

        self.input_data: str = input_data
        self.job_data = job_data
        self.logs = logs

        self.job_data.result = JobResult.unknown
        self.job_data.output_data = None

    @property
    def result(self):
        return self.job_data.result

    @result.setter
    def result(self, value):
        self.job_data.result = value

    @property
    def output_data(self):
        return self.job_data.output_data

    @output_data.setter
    def output_data(self, value):
        self.job_data.output_data = value
