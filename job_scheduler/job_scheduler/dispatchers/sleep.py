import time

from job_scheduler.dispatchers.base import Dispatcher
from job_scheduler.job import JobResult


class Sleep(Dispatcher):

    __type__ = "sleep"

    def run(self):
        time.sleep(int(self.input_data))
        self.logs.append(f">>Waiting for '{int(self.input_data)}' seconds")
        self.job_data.result = JobResult.success
