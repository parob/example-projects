import os

from graphql_api import GraphQLAPI
from graphql_http_server import GraphQLHTTPServer
from graphql_http_server.service_directory import ServiceDirectory, \
    ServiceConnection
from werkzeug import Request

from job_scheduler.pool import JobPool
from job_scheduler.worker import Worker

path = os.path.join(os.path.dirname(__file__), '../example_worker.graphql')
with open(path, mode='r') as file:
    default_query = file.read()

service_directory = ServiceDirectory(
    name=f"job_worker",
    api_version="0.0.1",
    connections=[ServiceConnection(
        name="job_pool",
        api_url="http://localhost:3501",
        stub=JobPool
    )]
)


worker = Worker(job_pool=service_directory["job_pool"])

graphql_http_server = GraphQLHTTPServer.from_api(
    api=GraphQLAPI(root=Worker),
    root_value=worker,
    graphiql_default_query=default_query
)


@service_directory.with_services
def main(request: Request):
    return graphql_http_server.dispatch(request=request)


if __name__ == "__main__":
    worker.start()

    graphql_http_server.run(main=main, port=int(os.getenv("PORT", 3502)))
