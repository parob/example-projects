import os

from graphql_api import GraphQLAPI
from graphql_http_server.service_directory import ServiceDirectory
from graphql_http_server import GraphQLHTTPServer
from sqlalchemy_gql import DatabaseManager
from werkzeug import Request

from job_scheduler.pool import JobPool

path = os.path.join(os.path.dirname(__file__), '../example_pool.graphql')
with open(path, mode='r') as file:
    default_query = file.read()

db_manager = DatabaseManager()
db_manager.install(False)

pool = JobPool()

graphql_http_server = GraphQLHTTPServer.from_api(
    api=GraphQLAPI(root=JobPool),
    root_value=pool,
    graphiql_default_query=default_query
)

service_directory = ServiceDirectory(
    name="job_pool",
    api_version="0.0.1",
    connections=[]
)


@service_directory.with_services
@db_manager.with_db_session
def main(request: Request):
    return graphql_http_server.dispatch(request=request)


if __name__ == "__main__":
    graphql_http_server.run(main=main, port=int(os.getenv("PORT", 3501)))
