import threading
import requests
import psutil

from dataclasses import dataclass, field as dataclass_field
from datetime import datetime
from typing import List, Type, Dict

from graphql import GraphQLError
from graphql_api import field
from graphql_api.relay import Node

from job_scheduler.dispatchers.base import Dispatcher
from job_scheduler.dispatchers import all_dispatchers
from job_scheduler.pool import JobPool
from job_scheduler.utils import ThreadSafeCounter


# noinspection PyAttributeOutsideInit
@dataclass
class Worker(Node):
    """
    The Worker will request and execute jobs from the pool
    """

    job_pool: JobPool
    status: str = "disconnected"
    heartbeat_rate: float = 500
    job_poll_rate: float = 1
    job_timeout: float = 60
    max_active_jobs: int = 1
    dispatchers: Dict = dataclass_field(default_factory=dict)

    @classmethod
    def graphql_exclude_fields(cls):
        return ["dispatchers"]

    def __post_init__(self):
        super().__init__()
        for dispatcher in all_dispatchers:
            if dispatcher.available():
                self.dispatchers[dispatcher.__type__] = dispatcher

        self.active_job_counter = ThreadSafeCounter(0)
        self.can_reach_pool = False
        self.stop_event = threading.Event()

    def send_heartbeat(self):
        while not self.stop_event.wait(self.heartbeat_rate):
            if not self.job_pool:
                raise EnvironmentError(
                    "Job pool is none, cannot send heartbeat."
                )

            if self.status == "disconnected":
                print(
                    f"Worker {self.id} Connecting "
                    f"to Job Pool {self.job_pool}"
                )

            heartbeat_success = self.job_pool.heartbeat(
                worker_id=self.id,
                cpu_percent=self.cpu_percent(),
                memory_percent=self.memory_percent()
            )

            if self.status == "disconnected" and heartbeat_success:
                print(
                    f"Worker {self.id} Connected "
                    f"to Job Pool {self.job_pool}"
                )

            if self.status == "connected" and not heartbeat_success:
                print(
                    f"Worker {self.id} Disconnected from "
                    f"Job Pool {self.job_pool}"
                )

            self.status = "connected" if heartbeat_success else "disconnected"

    def fetch_job(self):
        while not self.stop_event.wait(self.job_poll_rate):
            try:
                if self.active_jobs < self.max_active_jobs:
                    self.can_reach_pool = True

                    if not self.job_pool:
                        raise EnvironmentError(
                            "Job pool is none, cannot fetch job."
                        )
                    job_types = list(self.dispatchers.keys())
                    job_id = self.job_pool.request_job(types=job_types)

                    if job_id:
                        counter_incremented = False
                        try:
                            job = self.job_pool.job(id=job_id)
                            print(
                                f"Dispatching job id:'{job.id}' "
                                f"type:'{job.type}'"
                            )
                            self.active_job_counter.increment()
                            counter_incremented = True

                            def on_complete(_job):
                                print(
                                    f"Completed job id: '{_job.id}' type: "
                                    f"'{_job.type}' result: '{_job.result}'"
                                )
                                self.active_job_counter.decrement()
                                self.fetch_job()

                            timeout = self.job_timeout

                            dispatcher: Type[Dispatcher] = self.dispatcher(
                                job.type
                            )

                            process_thread = threading.Thread(
                                target=dispatcher.dispatch,
                                args=(job, timeout, on_complete),
                                daemon=True
                            )
                            process_thread.start()

                        except Exception as _:
                            self.job_pool.return_job(id=job_id)

                            if counter_incremented:
                                self.active_job_counter.decrement()

                            self.fetch_job()

            except (ConnectionError, requests.exceptions.ConnectionError) as e:
                self.can_reach_pool = False
                print(f"Poller connection error API error: {e}")

            except GraphQLError as err:
                self.can_reach_pool = False
                print(f"Poller GraphQL error API error: {err}")

    def dispatcher(self, job_type: str) -> Type[Dispatcher]:
        if job_type not in self.dispatchers.keys():
            print(f"Unable to dispatch job type {job_type}.")
            raise KeyError(f"Unable to dispatch job type {job_type}.")

        return self.dispatchers.get(job_type)

    @field(mutable=True)
    def start(self) -> bool:
        """
        Start the poller to register with the pool to request and execute jobs.
        """
        self.job_poller = threading.Thread(
            target=self.fetch_job,
            daemon=True
        )

        self.heartbeat_poller = threading.Thread(
            target=self.send_heartbeat,
            daemon=True
        )

        self.stop_event.clear()
        self.job_poller.start()
        self.heartbeat_poller.start()

        print(f"Started worker {self.id}")

        return True

    @field(mutable=True)
    def stop(self) -> bool:
        """
        Stop the worker from polling
        """
        self.stop_event.set()

        print(f"Stopped worker {self.id}")

        return True

    @field
    def active(self) -> bool:
        """
        State of this worker
        """
        return self.heartbeat_poller.is_alive and \
            self.job_poller.is_alive and \
            self.can_reach_pool

    @field
    def job_types(self) -> List[str]:
        """
        Job types this worker can handle.
        """
        return list(self.dispatchers.keys())

    @property
    @field
    def active_jobs(self) -> int:
        """
        Count of active jobs this worker is currently handling
        """
        return self.active_job_counter.value()

    @field
    def boot_time(self) -> str:
        """
        The date and time the system was booted
        """
        return datetime.fromtimestamp(psutil.boot_time()).strftime(
            '%Y-%m-%d %H:%M:%S'
        )

    @field
    def cpu_count(self) -> float:
        """
        CPUs available
        """
        return psutil.cpu_count()

    @field
    def cpu_percent(self) -> float:
        """
        CPU percent used of this workers process
        """
        return psutil.cpu_percent(interval=0.1)

    @field
    def memory_percent(self) -> float:
        """
        Memory percent used of this workers process
        """
        return psutil.virtual_memory().percent

    @field
    def memory_total(self) -> float:
        """
        Total memory of this workers machine
        """
        return psutil.virtual_memory().total

    @field
    def memory_used(self) -> float:
        """
        Used memory of this workers machine
        """
        return psutil.virtual_memory().used

    @field
    def memory_available(self) -> float:
        """
        Available memory of this workers machine
        """
        return psutil.virtual_memory().available
