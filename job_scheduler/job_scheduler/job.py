import enum

from datetime import datetime
from typing import List, Optional

from graphql_api import field
from sqlalchemy import Column, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.event import listens_for
from sqlalchemy_gql import ModelBase
from sqlalchemy_orm import UUIDType
from sqlalchemy_orm.filter import Filter


class JobState(enum.Enum):
    """
    The `JobState` EnumType represents the state of a `Job`.
    """
    created = "created"
    queued = "queued"
    allocated = "allocated"
    running = "running"
    completed = "completed"
    cancelled = "cancelled"


class JobResult(enum.Enum):
    """
    The `TaskResults` EnumType provide a high level outcome of a `Job`.
    """
    success = "success"
    failure = "failure"
    timeout = "timeout"
    unknown = "unknown"


class JobFilter(Filter):
    """
    The `JobFilter` ObjectType represents a set of attributes for
    filtering an Job search.
    """

    def __init__(self,
                 states: List[JobState] = None,
                 types: List[str] = None,
                 or_: List['JobFilter'] = None,
                 and_: List['JobFilter'] = None):
        """
        The `JobFilterInput` ObjectType represents a set of attributes for
        filtering an Job search.
        """
        super().__init__(or_=or_, and_=and_)
        self.states = states
        self.types = types

    def criterion(self, entities=None):
        criterion = super().criterion(entities=entities)

        if self.states is not None:
            criterion.append(Job.state.in_(self.states))

        if self.types is not None:
            criterion.append(Job.type.in_(self.types))

        return criterion


class Job(ModelBase):
    """
    The `Job` ObjectType represents a scheduled job.
    """

    type: str
    timeout: int = 1800
    state: JobState

    result: Optional[JobResult]
    log_data: str = ""

    input_data: str
    output_data: Optional[str]

    created_at: datetime
    queued_at: Optional[datetime]
    allocated_at: Optional[datetime]
    running_at: Optional[datetime]
    completed_at: Optional[datetime]

    trigger_id = Column(UUIDType, ForeignKey('job.id'))
    trigger = relationship("Job", uselist=False, foreign_keys='Job.trigger_id')

    @classmethod
    def graphql_from_input(cls, type: str, input_data: str = None):
        job = Job(type=type, input_data=input_data)
        return job

    def __init__(
        self,
        type: str,
        input_data: str,
        state: JobState = JobState.created,
        **kwargs
    ):
        super().__init__(**kwargs)
        self.type = type
        self.input_data = input_data
        self.state = state
        self.created_at = datetime.utcnow()

    @property
    @field
    def queue_time(self) -> Optional[float]:
        """
        The total time the job was queued for,
        null if the job has not yet started running
        """
        if self.queued_at and self.running_at:
            return (self.running_at - self.queued_at).total_seconds()

    @property
    @field
    def execution_time(self) -> Optional[float]:
        """
        The total time the job was executed for,
        null if the job has not yet completed
        """
        if self.running_at and self.completed_at:
            return (self.completed_at - self.running_at).total_seconds()

    @field(mutable=True)
    def progress(self, state: JobState = None) -> 'Job':
        """
        Update a `Job`s `JobState`
        """

        if state is not None:
            self.state = state

        return self

    # @query
    # def logs(self) -> List[str]:
    #     return self.log_data.split("\n")

    @field(mutable=True)
    def update_logs(self, logs: List[str]) -> 'Job':
        self.log_data += "\n".join(logs)
        return self

    @field(mutable=True)
    def complete(
        self,
        result: JobResult,
        output_data: str = None
    ) -> 'Job':
        """
        Set a `Job` as completed
        """
        self.result = result

        print(
            f"Completed job id: '{self.id}' type: '{self.type}'"
            f" result: '{self.result}'"
        )

        if output_data is not None:
            self.output_data = output_data

        self.state = JobState.completed

        return self

    @field(mutable=True)
    def delete(self, session=None) -> bool:
        return super().delete(session=session)


@listens_for(Job.state, 'set')
def job_state_update(job, value, oldvalue, initiator):
    """
    Set `Job` state and update corresponding time
    """
    job_state_map = {
        JobState.created: 'created_at',
        JobState.queued: 'queued_at',
        JobState.allocated: 'allocated_at',
        JobState.running: 'running_at',
        JobState.completed: 'completed_at',
        JobState.cancelled: 'cancelled_at'
    }

    key = job_state_map.get(value)

    if hasattr(job, key) and not getattr(job, key):
        setattr(job, key, datetime.utcnow())

    if value == JobState.completed or value == JobState.cancelled:
        if job.trigger and job.trigger.state == JobState.created:
            job.trigger.state = JobState.queued \
                if job.result == JobResult.success else JobState.cancelled
