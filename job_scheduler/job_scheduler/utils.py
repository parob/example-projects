from multiprocessing import Value, Lock


def get_all_subclasses(cls):
    all_subclasses = []

    for subclass in cls.__subclasses__():
        all_subclasses.append(subclass)
        all_subclasses.extend(get_all_subclasses(subclass))

    return all_subclasses


class ThreadSafeCounter:

    def __init__(self, initial_value=0):
        self._value = Value('i', initial_value)
        self.lock = Lock()

    def increment(self):
        with self.lock:
            self._value.value += 1

    def decrement(self):
        with self.lock:
            self._value.value -= 1

    def value(self):
        with self.lock:
            return self._value.value
