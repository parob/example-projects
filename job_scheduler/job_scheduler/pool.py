from datetime import datetime, timedelta
from dataclasses import dataclass

from uuid import UUID
from typing import List, Optional, Dict


from graphql import GraphQLError
from graphql_api import field
from graphql_api.relay import Node
from sqlalchemy_orm.order_by import OrderBy, OrderByDirection

from job_scheduler.job import Job, JobFilter, JobState


@dataclass
class WorkerInfo(Node):
    """
    Basic information on a worker.
    """
    id: UUID
    last_heartbeat: datetime = None
    cpu_percent: float = 0
    memory_percent: float = 0


class JobPool(Node):

    def __init__(
        self,
        heartbeat_timeout: int = 10
    ):
        super().__init__()
        self._worker_info: Dict[UUID, WorkerInfo] = {}
        self.heartbeat_timeout = heartbeat_timeout

    @field
    def job(self, id: UUID) -> Job:
        """
        Get a `Job` by its ID
        """
        job = Job.get(id=id)

        if not job:
            raise GraphQLError(f"Job id:'{id}' could not be found.")

        return job

    @field
    def average_cpu_percent(self) -> float:
        """
        Get the average CPU percent of all the workers
        """
        worker_info = self.worker_info()
        worker_count = len(worker_info)

        if worker_count == 0:
            return 0

        return sum([
            info.cpu_percent
            for info in worker_info
        ]) / worker_count

    @field
    def average_memory_percent(self) -> float:
        """
        Get the average memory percent of all the workers
        """
        worker_info = self.worker_info()
        worker_count = len(worker_info)

        if worker_count == 0:
            return 0

        return sum([
            info.memory_percent
            for info in worker_info
        ]) / worker_count

    @field
    def all_jobs(
        self,
        limit: int = 50,
        offset: int = 0,
        order_by: List[OrderBy] = None,
        filter: JobFilter = None
    ) -> List[Job]:
        """
        Get all `Job`s
        """

        if order_by is None:
            order_by = [
                OrderBy(
                    key='created_at',
                    direction=OrderByDirection.asc
                )
            ]

        job_query = Job.query()

        if filter:
            job_query = job_query.filter(filter)

        if order_by:
            job_query = job_query.order_by(*order_by)

        if offset:
            job_query = job_query.offset(offset)

        if limit:
            job_query = job_query.limit(limit)

        return job_query.all()

    def remove_timed_out_workers(self):
        now_less_timeout = datetime.utcnow() - timedelta(
            seconds=self.heartbeat_timeout
        )

        self._worker_info = {
            worker_id: worker_info
            for worker_id, worker_info in self._worker_info.items()
            if worker_info.last_heartbeat > now_less_timeout
        }

    @field
    def worker_info(self) -> List[WorkerInfo]:
        """
        Get all available workers
        """
        self.remove_timed_out_workers()

        return list(self._worker_info.values())

    @field(mutable=True)
    def create_job(self, job: Job, queue: bool = True) -> Job:
        """
        Create a `Job` and add it to the back of the queue
        """
        if queue:
            job.state = JobState.queued

        job.create()

        print(f"Created job id: '{job.id}' type: '{job.type}'")

        return job

    @field(mutable=True)
    def create_job_chain(
        self,
        jobs: List[Job],
        queue: bool = True
    ) -> List[Job]:
        """
        Create a `Job` chain and add the first job to the back of the queue
        """
        if len(jobs) == 0:
            return []

        index = 1
        for job in jobs:

            if index == 1 and queue:
                job.state = JobState.queued
            else:
                job.state = JobState.created

            if len(jobs) > index:
                job.trigger = jobs[index]

            job.create()

            index += 1

        print(f"Created job chain of {len(jobs)} jobs {jobs}.")

        return jobs

    @field(mutable=True)
    def heartbeat(
        self,
        worker_id: UUID,
        cpu_percent: float = 0,
        memory_percent: float = 0
    ) -> bool:
        """
        Used to let the pool know a worker is still alive.
        """
        if worker_id not in self._worker_info.keys():
            print(f"Connected with worker {worker_id}")

        self._worker_info[worker_id] = \
            WorkerInfo(
                id=worker_id,
                last_heartbeat=datetime.utcnow(),
                cpu_percent=cpu_percent,
                memory_percent=memory_percent
            )

        return True

    @field(mutable=True)
    def request_job(self, types: List[str] = None) -> Optional[UUID]:
        """
        Take a `Job` off the front of the queue, mark it as allocated,
        and return its ID
        """
        filter = JobFilter(types=types, states=[JobState.queued])

        order_by = OrderBy(
            key='created_at',
            direction=OrderByDirection.asc
        )

        job = Job.filter(filter).order_by(order_by).limit(1).one_or_none()

        if job:
            job.state = JobState.allocated
            print(f"Allocated job id: '{job.id}' type: '{job.type}'")
            return job.id

    @field(mutable=True)
    def return_job(self, id: UUID) -> bool:
        """
        Return `Job` back to the queue.
        """
        job = Job.filter(id=id).one_or_none()

        if not job:
            print(
                f"Attempted to return a job that could not be found. "
                f"Job id: '{job.id}' type: '{job.type}'"
            )
            return False
        if job.state != JobState.allocated:
            print(
                f"Attempted to return a job that was not allocated. "
                f"Job id: '{job.id}' type: '{job.type}'"
            )
            return False
        else:
            job.state = JobState.queued
            job.allocated_at = None
            print(f"Queued job id: '{job.id}' type: '{job.type}'")
        return True

