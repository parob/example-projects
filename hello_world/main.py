from graphql_api import GraphQLAPI, field
from graphql_http_server import GraphQLHTTPServer


class HelloWorld:

    @field
    def hello_world(self) -> str:
        return "Hello world!"


server = GraphQLHTTPServer.from_api(api=GraphQLAPI(root=HelloWorld))


# define main fuction for Google Cloud Function support
def main(request):
    return server.dispatch(request=request)


# define app for Dockerfile/gunicorn support
app = server.app(main=main)

if __name__ == "__main__":
    server.run(main=main, port=3502)
