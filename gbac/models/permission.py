from dataclasses import dataclass
from typing import List

from models.relation import Relation
from models.resource import Resource


@dataclass
class Permission:
    start: Resource = None
    end: Resource = None
    activity: str = None
    chain: List[Relation] = None

    def __hash__(self):
        return hash(f"{self.start.id}{self.end.id}{self.activity}")
