from dataclasses import dataclass
from typing import Optional

from sqlalchemy_gql.orm_base import ModelBase


@dataclass
class Resource(ModelBase):
    type: Optional[str] = None

    def __hash__(self):
        return hash(self.id)