from dataclasses import dataclass
from typing import Optional

from sqlalchemy_gql.orm_base import ModelBase

from models.resource import Resource

ACTIVITY_WILDCARD = "*"


@dataclass
class Relation(ModelBase):
    start: Resource = None
    end: Resource = None
    activity: str = None
    emulate: bool = False
    authority: Optional[Resource] = None
