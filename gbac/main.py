import os

from graphql_api import GraphQLAPI
from graphql_http_server import GraphQLHTTPServer
from sqlalchemy_gql.orm_base import DatabaseManager
from werkzeug import Response

from controllers.access import AccessControl

# Check if we are deployed in Google Cloud Functions
is_google_cloud_function = os.getenv("FUNCTION_SIGNATURE_TYPE", os.getenv("FUNCTION_TRIGGER_TYPE")) == "http"


# Use an in-memory database in Google Cloud Functions
if is_google_cloud_function:
    db_manager = DatabaseManager(url="sqlite:///:memory:")
else:
    db_manager = DatabaseManager()
    db_manager.install()


path = os.path.join(os.path.dirname(__file__), './example.graphql')
with open(path, mode='r') as file:
    default_query = file.read()

server = GraphQLHTTPServer.from_api(
    api=GraphQLAPI(root=AccessControl),
    graphiql_default_query=default_query
)


@db_manager.with_db_session
def main(request):

    # Reinstall the models if in Google Cloud Functions and DB is empty
    if is_google_cloud_function and db_manager.db.is_empty():
        db_manager.install()

    # Server the entity relationship diagram if reqested
    if request.path.endswith("/db_schema"):
        return db_manager.db.entity_relationship_diagram()

    return server.dispatch(request=request)


if __name__ == "__main__":
    server.run(main=main, port=3501)

