import uuid

from models.relation import ACTIVITY_WILDCARD


class TestGBACService:

    def test_no_permission(self, db_ctx, access_control):
        resource_1 = uuid.uuid4()
        resource_2 = uuid.uuid4()

        with db_ctx:
            assert not access_control.get_permission(
                resource_1,
                resource_2,
                "ACCESS"
            )

    def test_create_permission(self, db_ctx, access_control):

        with db_ctx:
            resource_1 = access_control.create_resource()
            resource_2 = access_control.create_resource()

            access_control.create_relation(
                resource_1.id,
                resource_2.id,
                "ACCESS"
            )

            permission = access_control.get_permission(
                resource_1.id,
                resource_2.id,
                activity="ACCESS"
            )

            assert permission

    def test_delete_permission(self, db_ctx, access_control):

        with db_ctx:
            resource_1 = access_control.create_resource()
            resource_2 = access_control.create_resource()

            relationship = access_control.create_relation(
                resource_1.id,
                resource_2.id,
                "ACCESS"
            )

            assert access_control.get_permission(
                resource_1.id,
                resource_2.id,
                "ACCESS"
            )

            relationship.delete()

            assert not access_control.get_permission(
                resource_1.id,
                resource_2.id,
                "ACCESS"
            )

    def test_wildcard_permission(self, db_ctx, access_control):

        with db_ctx:
            resource_1 = access_control.create_resource()
            resource_2 = access_control.create_resource()

            access_control.create_relation(
                resource_1.id,
                resource_2.id,
                ACTIVITY_WILDCARD
            )

            assert access_control.get_permission(
                resource_1.id,
                resource_2.id,
                "ACCESS"
            )

    def test_self_permission(self, db_ctx, access_control):

        with db_ctx:
            resource_1 = access_control.create_resource()

            assert access_control.get_permission(
                resource_1.id,
                resource_1.id,
                "ACCESS"
            )

    def test_atomic_permission(self, db_ctx, access_control):
        with db_ctx:
            resource_1 = access_control.create_resource()
            resource_2 = access_control.create_resource()

            access_control.create_relation(
                resource_1.id,
                resource_2.id,
                "ACCESS"
            )

            assert not access_control.get_permission(
                resource_1.id,
                resource_2.id,
                "NOT_ACCESS"
            )

    def test_reverse_permission(self, db_ctx, access_control):

        with db_ctx:
            resource_1 = access_control.create_resource()
            resource_2 = access_control.create_resource()

            access_control.create_relation(
                resource_1.id,
                resource_2.id,
                "ACCESS"
            )

            assert not access_control.get_permission(
                resource_2.id,
                resource_1.id,
                "ACCESS"
            )

    def test_forward_permission(self, db_ctx, access_control):

        with db_ctx:
            resource_1 = access_control.create_resource()
            resource_2 = access_control.create_resource()
            resource_3 = access_control.create_resource()

            access_control.create_relation(
                resource_1.id,
                resource_2.id,
                "ACCESS",
                emulate=True
            )
            access_control.create_relation(
                resource_2.id,
                resource_3.id,
                "ACCESS"
            )

            assert access_control.get_permission(
                resource_1.id,
                resource_3.id,
                "ACCESS"
            )

    def test_restricted_permission(self, db_ctx, access_control):

        with db_ctx:
            resource_1 = access_control.create_resource()
            resource_2 = access_control.create_resource()
            resource_3 = access_control.create_resource()

            access_control.create_relation(
                resource_1.id,
                resource_2.id,
                "ACCESS"
            )
            access_control.create_relation(
                resource_2.id,
                resource_3.id,
                "ACCESS"
            )

            assert not access_control.get_permission(
                resource_1.id,
                resource_3.id,
                "ACCESS"
            )

    def test_recursion_limit_permission(self, db_ctx, access_control):

        with db_ctx:
            resources = [access_control.create_resource() for _ in range(8)]

            previous = resources[0]
            for resource in resources[1:]:
                access_control.create_relation(
                    start_id=previous.id,
                    end_id=resource.id,
                    activity="ACCESS",
                    emulate=True
                )

                previous = resource

            assert access_control.get_permission(
                resources[0].id,
                resources[6].id,
                "ACCESS"
            )
            assert not access_control.get_permission(
                resources[0].id,
                resources[7].id,
                "ACCESS"
            )
            assert access_control.get_permission(
                resources[0].id,
                resources[7].id,
                "ACCESS",
                max_depth=7
            )

    def test_resource_find(self, db_ctx, access_control):

        with db_ctx:
            resource_1 = access_control.create_resource(type="Person")
            resource_2 = access_control.create_resource(type="Person")
            resource_3 = access_control.create_resource(type="House")
            resource_4 = access_control.create_resource(type="Account")
            resource_5 = access_control.create_resource(type="Account")

            access_control.create_relation(
                start_id=resource_1.id,
                end_id=resource_3.id,
                activity="ACCESS",
                emulate=True
            )

            access_control.create_relation(
                start_id=resource_1.id,
                end_id=resource_2.id,
                activity="ACCESS",
                emulate=True
            )

            access_control.create_relation(
                start_id=resource_2.id,
                end_id=resource_4.id,
                activity="ACCESS",
                emulate=True
            )

            access_control.create_relation(
                start_id=resource_3.id,
                end_id=resource_4.id,
                activity="ACCESS"
            )

            access_control.create_relation(
                start_id=resource_1.id,
                end_id=resource_5.id,
                activity="ACCESS"
            )

            permissions = access_control.all_permissions(
                start_id=resource_1.id,
                activity="ACCESS",
                types={"Account"}
            )

            resources = {permission.end for permission in permissions}

            assert resources == {resource_4, resource_5}
