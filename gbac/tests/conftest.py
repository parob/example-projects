import pytest

from context_helper.context import Context
from sqlalchemy_gql.orm_base import DatabaseManager

from main import db_manager as _db_manager
from controllers.access import AccessControl


@pytest.fixture(scope="session")
def db_manager():
    return _db_manager


@pytest.fixture
def access_control() -> AccessControl:
    return AccessControl()


@pytest.fixture
def db_session(db_manager: DatabaseManager):
    session = db_manager.db.session()
    yield session
    session.commit()


@pytest.fixture
def db_ctx(db_session):
    return Context(clone=False, db_session=db_session)
