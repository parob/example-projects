from typing import Optional
from typing import List
from uuid import UUID

from graphql_api import field
from sqlalchemy import and_, or_

from models.resource import Resource
from models.permission import Permission
from models.relation import Relation, ACTIVITY_WILDCARD


class AccessControl:

    @field(mutable=True)
    def create_resource(self, id: UUID = None, type: str = None) -> Resource:
        resource = Resource(type=type)
        if id:
            resource.id = id

        resource.create()
        return resource

    @field
    def get_resource(self, id: UUID) -> Resource:
        return Resource.get(id)

    @field
    def all_resources(self, types: List[str] = None) -> List[Resource]:
        query = Resource.query()

        if types:
            query = query.filter(Resource.type.in_(types))

        return query.all()

    @field(mutable=True)
    def create_relation(
        self,
        start_id: UUID,
        end_id: UUID,
        activity: str,
        emulate: bool = False,
        authority_id: UUID = None
    ) -> Relation:
        start = Resource.get(id=start_id)
        end = Resource.get(id=end_id)

        relation = Relation(
            start=start,
            end=end,
            activity=activity,
            emulate=emulate
        )

        if authority_id:
            authority = Resource.get(id=authority_id)
            relation.authority = authority

        relation.create()
        return relation

    @field
    def get_relation(self, id: UUID) -> Relation:
        return Relation.get(id)

    @field
    def get_permission(
        self,
        start_id: UUID,
        end_id: UUID,
        activity: str,
        max_depth: int = 6
    ) -> Optional[Permission]:
        return self.get_permission(start_id, end_id, activity, max_depth)

    @field
    def all_permissions(
        self,
        start_id: UUID = None,
        activity: str = None,
        types: List[str] = None,
        max_depth: int = 6
    ) -> List[Permission]:
        return self.all_permissions(start_id, activity, types, max_depth)

    """
    Recursive get permission
    """
    def _get_permission(
        self,
        start_id: UUID,
        end_id: UUID,
        activity: str,
        max_depth: int = 6,
        depth: int = 0,
        chain=None,
        checked_ids=None
    ) -> Optional[Permission]:

        if depth >= max_depth:
            return

        if not checked_ids:
            checked_ids = set()

        if not chain:
            chain = []

        if start_id == end_id:
            resource = Resource.get(start_id)
            if not resource:
                return
            return Permission(
                start=resource,
                end=resource,
                activity=activity,
                chain=chain
            )

        relations = Relation.query().filter(
            and_(Relation.start_id == start_id,
                 or_(Relation.activity == activity,
                     Relation.activity == ACTIVITY_WILDCARD)
                 )).all()

        for relation in relations:
            chain = [*chain, relation]
            if relation.end_id == end_id:
                return Permission(
                    start=Resource.get(start_id),
                    end=Resource.get(end_id),
                    activity=activity,
                    chain=chain
                )
            if relation.start_id not in checked_ids and relation.emulate:
                checked_ids.add(relation.start_id)
                permission = self._get_permission(
                    relation.end_id,
                    end_id,
                    activity,
                    max_depth,
                    depth + 1,
                    chain=chain,
                    checked_ids=checked_ids
                )
                if permission:
                    return permission

    """
    Recursive all permissions
    """
    def _all_permissions(
        self,
        start_id: UUID = None,
        activity: str = None,
        types: List[str] = None,
        max_depth: int = 6,
        depth=0,
        permissions=None,
        chain=None,
        checked_ids=None
    ) -> List[Permission]:

        if depth >= max_depth:
            return []

        if not checked_ids:
            checked_ids = set()

        if not chain:
            chain = []

        if not permissions:
            permissions = set()

        relations = Relation.query().filter(
            and_(Relation.start_id == start_id,
                 or_(Relation.activity == activity,
                     Relation.activity == ACTIVITY_WILDCARD)
                 )).all()

        for relation in relations:
            chain = [*chain, relation]

            if not types or relation.end.type in types:
                permissions.add(Permission(
                    start=Resource.get(start_id),
                    end=Resource.get(relation.end_id),
                    activity=activity,
                    chain=chain
                ))
            if relation.start_id not in checked_ids and relation.emulate:
                checked_ids.add(relation.start_id)
                permissions.update(self._all_permissions(
                    relation.end_id,
                    activity,
                    types,
                    max_depth,
                    depth + 1,
                    permissions,
                    chain=chain,
                    checked_ids=checked_ids
                ))

        return permissions
