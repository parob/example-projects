WITH RECURSIVE traverse(id) AS (
  SELECT ?
  UNION
  SELECT start FROM relation JOIN traverse ON end = id
) SELECT id FROM traverse;
