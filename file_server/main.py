import os

from typing import List
from uuid import UUID

from graphql_api import field, GraphQLAPI
from sqlalchemy_orm.order_by import OrderBy, OrderByDirection

from graphql_http_server import GraphQLHTTPServer

from sqlalchemy_gql.orm_base import DatabaseManager


from file import FileType, File, FileFilter
from werkzeug import Response


class FileServerController:

    @field(mutable=True)
    def create_file(
        self,
        name: str,
        type: FileType = FileType.file,
        data: str = None,
        parent_id: UUID = None
    ) -> File:
        """
        Create `File`
        """

        parent = File.get(parent_id)

        if parent_id and not parent:
            raise NameError(f"Parent `Folder` with id: '{parent_id}' does not exist.")

        file = File(
            name=name,
            type=type,
            data=data,
            parent=parent
        )
        file.create()
        return file

    @field
    def file(self, id: UUID) -> File:
        """
        Find a specific `File` by its ID.
        """
        app = File.get(id)

        if not app:
            raise NameError(f"`File` with id: '{id}' does not exist.")

        return app

    @field
    def all_files(
        self,
        order_by: List[OrderBy] = None,
        filter: FileFilter = None,
        limit: int = 50,
        offset: int = 0
    ) -> List[File]:
        """
        Find `Files`
        """

        if order_by is None:
            order_by = [
                OrderBy('name', OrderByDirection.asc)
            ]

        return File \
            .query() \
            .filter(filter) \
            .order_by(*order_by) \
            .offset(offset) \
            .limit(limit) \
            .all()


# Check if we are deployed in Google Cloud Functions
is_google_cloud_function = os.getenv("FUNCTION_SIGNATURE_TYPE", os.getenv("FUNCTION_TRIGGER_TYPE")) == "http"

if is_google_cloud_function:     # Use an in-memory database in Google Cloud Functions
    db_manager = DatabaseManager(url="sqlite:///:memory:")
else:
    db_manager = DatabaseManager()
    db_manager.install()


path = os.path.join(os.path.dirname(__file__), './example.graphql')
with open(path, mode='r') as file:
    default_query = file.read()

server = GraphQLHTTPServer.from_api(api=GraphQLAPI(
    root=FileServerController),
    graphiql_default_query=default_query
)


@db_manager.with_db_session
def main(request):

    # Reinstall the models if in Google Cloud Functions and DB is empty
    if is_google_cloud_function and db_manager.db.is_empty():
        db_manager.install()

    if request.path.lower() == "/schema":
        return Response(db_manager.db.entity_relationship_diagram(), content_type="text/html")

    return server.dispatch(request=request)


if __name__ == "__main__":
    server.run(main=main, port=3503)
