import enum
import io
import os
import time
import zipfile
from dataclasses import dataclass

from typing import List, Union
from uuid import UUID

from graphql_api import field
from sqlalchemy_gql.orm_base import ModelBase

from sqlalchemy import Column, ForeignKey, UniqueConstraint, event, LargeBinary
from sqlalchemy.orm import relationship

from sqlalchemy_orm import UUIDType
from sqlalchemy_orm.filter import Filter


class FileType(enum.Enum):
    """
    `FileType`
    """
    folder = "folder"
    file = "file"


@dataclass
class FileFilter(Filter):
    """
    The `FileFilterInput` ObjectType represents a set of attributes for
    filtering an `File` search.

    Attributes:
        name: The name of the `File`.
        type: The file type of the `File`.
        parent_id: The parent_id of the `File`.
    """
    ids: List[UUID] = None
    name: str = None
    type: FileType = None
    parent_id: UUID = None

    def criterion(self, entities=None):
        criterion = super().criterion(entities=entities)

        if self.ids:
            criterion.append(File.id.in_(self.ids))

        if self.name:
            criterion.append(File.name == self.name)

        if self.type:
            criterion.append(File.type == self.type)

        if self.parent_id:
            criterion.append(File.parent_id == self.parent_id)

        return criterion


class File(ModelBase):
    """
    `File`
    """
    name: str
    type: FileType
    data = Column(LargeBinary)
    parent_id = Column(UUIDType, ForeignKey("file.id"))

    parent = relationship(
        "File",
        remote_side="File.id",
        foreign_keys=parent_id,
        back_populates="children"
    )

    children = relationship(
        "File",
        cascade="all, delete-orphan, save-update",
        foreign_keys=parent_id,
        back_populates="parent"
    )

    __table_args__ = (
        UniqueConstraint('name', 'parent_id', name='_parent_id_name_uc'),
    )

    @classmethod
    def graphql_exclude_fields(cls) -> List[str]:
        return ['parent_id']

    @classmethod
    def folder(cls, name, children: List['File'] = None) -> 'File':
        return File(name=name, type=FileType.folder, children=children)

    @classmethod
    def file(cls, name, data: Union[bytes, str] = None) -> 'File':
        return File(name=name, type=FileType.file, data=data)

    def __init__(
        self,
        name: str,
        type: FileType = FileType.file,
        data: Union[bytes, str] = None,
        parent: 'File' = None,
        children: List['File'] = None, *args, **kwargs
    ):

        self.type = type
        super().__init__(*args, **kwargs)

        if isinstance(data, str):
            data = str.encode(data)

        self.name = name

        if data is not None:
            self.data = data

        if parent is not None:
            self.parent = parent

        if children is not None:
            for child in children:
                self.children.append(child)

    def __getitem__(self, filename):
        """
        folder[filename] notation
        """
        if not self.type == FileType.folder:
            raise TypeError(
                "File '{target}' is not a folder, cannot use folder.get."
            )

        for file in self.children:
            if file.name == filename:
                return file

        raise AttributeError(f"File '{filename}' not found in folder '{self}'")

    def __contains__(self, file):
        """
        Python 'file in folder' notation
        """
        if not self.type == FileType.folder:
            raise TypeError(
                "File '{target}' is not a folder and cannot contain values."
            )

        return file in self.children

    def __iter__(self):
        """
        Python 'for file in folder' notation
        """
        if not self.type == FileType.folder:
            raise TypeError(
                "File '{target}' is not a folder and cannot be iterated."
            )

        for elem in self.children:
            yield elem

    @field
    def size(self) -> int:
        """
        The size of this `File` in bytes
        """
        if self.data is None:
            return 0

        return len(self.data)

    @field
    def full_path(self) -> str:
        """
        The full path of this `File`
        """
        data = self
        file_path = os.path.sep + (data.name if data.name else "")
        while data.parent:
            data = data.parent
            if hasattr(data, 'name'):
                file_path = os.path.sep + data.name + file_path

        return file_path

    @field(mutable=True)
    def update(
            self,
            name: str = None,
            data: str = None
    ) -> 'File':
        """
        Updates a `File`
        """

        if name:
            self.name = name

        if data:
            self.data = data

        return self

    def zip(
            self,
            recursion_limit=5,
            size_limit=10485760,
            timeout=10
    ) -> io.BytesIO:
        """
        Compress the `File` into a zip file and return the bytes.
        """

        if self.type == FileType.folder:
            zip_data = io.BytesIO()
            zip_file = zipfile.ZipFile(zip_data, 'w', zipfile.ZIP_STORED)

            try:
                root_path = self.full_path()

                total_size = 0
                start = time.time()

                for file in self.walk(recursion_limit=recursion_limit):
                    total_size += file.size()

                    if total_size > size_limit:
                        raise MemoryError(
                            f"Folder '{self.name}' is to big to be Zipped. Zip"
                            f" limit is '{size_limit}' bytes."
                        )

                    if time.time() - start > timeout:
                        raise TimeoutError(
                            f"Folder '{self.name}' is took too long to be "
                            f"Zipped. Zip timeout is '{timeout}' seconds."
                        )

                    file_relative_path = file.full_path().replace(
                        root_path,
                        ""
                    )

                    if file_relative_path.startswith(os.sep):
                        file_relative_path = file_relative_path[1:]

                    if file.type == FileType.folder:
                        zif = zipfile.ZipInfo(file_relative_path + "/")
                        zip_file.writestr(zif, "")

                    else:
                        zip_file.writestr(
                            zinfo_or_arcname=file_relative_path,
                            data=file.data
                        )

            finally:
                zip_file.close()

            zip_data.seek(0)

            return zip_data

        elif self.type == FileType.file:
            raise NotImplementedError("File zipping not implemented yet.")

    def walk(self, recursion_limit=5, include_dirs=True, walked_data=None):
        """
        Iterate through the children of a `Folder`
        """

        if not walked_data:
            walked_data = set()

        for data in self.children:
            if data not in walked_data:
                walked_data.add(data)

                if include_dirs or data.type == FileType.file:
                    yield data

                if data.type == FileType.folder and recursion_limit > 0:
                    for _data in data.walk(recursion_limit=recursion_limit - 1,
                                           include_dirs=include_dirs,
                                           walked_data=walked_data):
                        yield _data


@event.listens_for(File.children, 'append')
def validate_children(target, value, oldvalue):
    """
    Stop `Files` having children
    """
    if target.type == FileType.file:
        raise TypeError(
            f"File '{target}' is not a '{FileType.folder}'"
            f" and cannot contain children."
        )


@event.listens_for(File.data, 'set')
def validate_data(target, value, oldvalue, initiator):
    """
    Stop `Folders` having data
    """
    if target.type == FileType.folder:
        raise TypeError(
            f"File '{target}' is not a '{FileType.file}'"
            f" and cannot have data."
        )


@event.listens_for(File.parent, 'set')
@event.listens_for(File.parent, 'set')
def validate_parent(target, value, oldvalue, initiator):
    """
    Stop cyclic `Folder` chains!
    """
    parent = value

    if parent.type == FileType.file:
        raise TypeError(
            f"File '{parent}' is not a '{FileType.folder}'"
            f" and cannot contain children."
        )

    while parent:
        if parent == target:
            raise ValueError(
                f"File '{target}' parent cannot be '{parent}', '{parent}'"
                f" is already a child of '{target}'. This would create"
                f" a cyclic folder structure."
            )
        parent = parent.parent
