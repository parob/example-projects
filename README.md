# Example Projects

These example projects are built using Parobs's open source libraries.
They all require Python 3 or above to be installed, and some require Python 3.7 for dataclass support.

### Installation

All of the examples can be ran by first installing the dependencies from *requirements.txt*:

```
pip install -r requirements.txt
```

and then running the main.py

```
python main.py
```
