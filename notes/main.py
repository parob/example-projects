import os

from dataclasses import dataclass
from typing import List
from uuid import UUID

from graphql_api import GraphQLAPI, field
from sqlalchemy_gql.orm_base import DatabaseManager, ModelBase

from sqlalchemy_orm.filter import Filter
from sqlalchemy_orm.order_by import OrderBy, OrderByDirection

from graphql_http_server import GraphQLHTTPServer


class Note(ModelBase):
    title: str = ""
    note: str = ""

    @field(mutable=True)
    def update(self, title: str = None, note: str = None) -> 'Note':
        if title is not None:
            self.title = title

        if note is not None:
            self.note = note

        return self


# noinspection DuplicatedCode
@dataclass
class NoteFilter(Filter):
    ids: List[UUID] = None
    title: str = ""

    def criterion(self, entities=None):
        criterion = super().criterion(entities=entities)

        if self.ids:
            criterion.append(Note.id.in_(self.ids))

        if self.title:
            criterion.append(Note.title.ilike('%' + self.title + '%'))

        return criterion


class Notes:

    @field(mutable=True)
    def create_note(self, title: str, note: str) -> Note:
        note = Note(title=title, note=note)
        note.create()
        return note

    @field
    def all_notes(
        self,
        order_by: List[OrderBy] = None,
        filter: NoteFilter = None,
        limit: int = 50,
        offset: int = 0
    ) -> List[Note]:
        """
        Find `Notes`
        """

        if order_by is None:
            order_by = [
                OrderBy('title', OrderByDirection.asc)
            ]

        return Note \
            .query() \
            .filter(filter) \
            .order_by(*order_by) \
            .offset(offset) \
            .limit(limit) \
            .all()

    @field
    def note(self, id: UUID = None, title: str = None) -> Note:
        """
        Title or id must be an exact match.
        """
        if id:
            return Note.query().filter(Note.id == id).one()
        if title:
            return Note.query().filter(Note.title == title).one()


# Check if we are deployed in Google Cloud Functions
is_google_cloud_function = os.getenv("FUNCTION_SIGNATURE_TYPE", os.getenv("FUNCTION_TRIGGER_TYPE")) == "http"

if is_google_cloud_function:     # Use an in-memory database in Google Cloud Functions
    db_manager = DatabaseManager(url="sqlite:///:memory:")
else:
    db_manager = DatabaseManager()
    db_manager.install()


path = os.path.join(os.path.dirname(__file__), './example.graphql')
with open(path, mode='r') as file:
    default_query = file.read()

server = GraphQLHTTPServer.from_api(
    api=GraphQLAPI(root=Notes),
    graphiql_default_query=default_query
)


@db_manager.with_db_session
def main(request):

    # Reinstall the models if in Google Cloud Functions and DB is empty
    if is_google_cloud_function and db_manager.db.is_empty():
        db_manager.install()
        
    return server.dispatch(request=request)


if __name__ == "__main__":
    server.run(main=main, port=3501)
