from typing import List, Optional

from datarm.order_by import OrderBy, OrderByDirection
from objectql import query, mutation

from user_directory.models.user import User, UserFilter


class UserController:
    """
    User management controller
    """

    @mutation
    def create_user(self, name: str, email: str, password: str) -> User:
        """
        Creates a new `User`
        """
        user = User(name=name, email=email, password=password)
        user.create()

        return user

    @query
    def user(self, filter: UserFilter) -> Optional[User]:
        """
        Finds a single `User` with a filter.
        """
        return User.query().filter(filter).one_or_none()

    @query
    def all_users(
        self,
        limit: int = 50,
        offset: int = 0,
        order_by: List[OrderBy] = None,
        filter: UserFilter = None
    ) -> List[User]:
        """
        Finds all `Users` with a filter.

        :param limit: The maximum number of `Users` that should be returned.
        :param offset: The offset, return `Users` after the nth (offset) `User`
        :param order_by:
        :param filter:
        :return:
        """
        if order_by is None:
            # noinspection PyArgumentList
            order_by = [OrderBy(key='name', direction=OrderByDirection.asc)]

        return User \
            .filter(filter) \
            .order_by(*order_by) \
            .offset(offset) \
            .limit(limit) \
            .all()
