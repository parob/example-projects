from objectql.schema import ObjectQLSchema
from graphql_http_server import GraphQLHTTPServer

from werkzeug.wrappers import Request

from objectql_datarm.db import DatabaseManager

from service_controller import RemoteServicePool
from user_directory.controllers.user import UserController

db_manager = DatabaseManager()

server = GraphQLHTTPServer.from_schema(
    schema=ObjectQLSchema(root=UserController)
)

service_controller = RemoteServicePool(
    name="user_directory",
    version="1.0.0"
)


@service_controller.with_services
@db_manager.with_db_session
def main(request: Request):
    return server.dispatch(request=request)


app = server.app(main=main)
