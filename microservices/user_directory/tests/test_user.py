import pytest

from user_management.controllers.user import UserController
from user_management.models.user import UserFilter, User


class TestUserController:

    def test_create_user(self, db_session_ctx):
        user_controller = UserController()

        user = user_controller.create_user(
            name="user_name",
            email="user_email@email.com",
            password="user_password"
        )

        user_id = user.id

        assert user in user_controller.all_users()
        assert user_controller.user(filter=UserFilter(id=user_id)) == user

    def test_create_user_duplicate(self, db_session_ctx):
        user_controller = UserController()

        user_controller.create_user(
            name="user_name",
            email="user_email@email.com",
            password="user_password"
        )

        with pytest.raises(NameError, match="already exists"):
            user_controller.create_user(
                name="user_name",
                email="user_email@email.com",
                password="user_password"
            )

    def test_create_user_invalid_email(self, db_session_ctx):
        user_controller = UserController()

        with pytest.raises(ValueError, match="invalid_email is invalid"):
            user_controller.create_user(
                name="user_name",
                email="invalid_email",
                password="user_password"
            )

    def test_get_user(self, db_session_ctx):
        user_controller = UserController()

        user = user_controller.create_user(
            name="user_name",
            email="user_email@email.com",
            password="user_password"
        )

        assert user == User.get(id=user.id)
        assert user == User.get(email=user.email)

    def test_user_password_is_encrypted(self, db_session_ctx):
        user_controller = UserController()

        test_password = "user_password"

        user = user_controller.create_user(
            name="user_name",
            email="user_email@email.com",
            password=test_password
        )

        assert user in user_controller.all_users()

        with pytest.raises(AttributeError):
            print(user.password)

        assert user.password_hash != test_password
        assert test_password not in user.password_hash.decode("utf-8")

    def test_user_check_password(self, db_session_ctx):
        user_controller = UserController()

        test_password = "user_password"

        user = user_controller.create_user(
            name="user_name",
            email="user_email@email.com",
            password=test_password
        )

        assert not user.check_password(password="invalid_password")
        # noinspection PyTypeChecker
        assert not user.check_password(password=None)
        assert not user.check_password(password="")
        assert user.check_password(password=test_password)

    def test_user_update(self, db_session_ctx):
        user_controller = UserController()

        name = "user_name"
        email = "user_email@email.com"
        password = "user_password"

        user = user_controller.create_user(
            name=name,
            email=email,
            password=password
        )

        user_id = user.id
        user_password_hash = user.password_hash

        updated_user = user_controller.user(filter=UserFilter(id=user_id))

        updated_name = "updated_name"
        updated_email = "updated_email@email.com"
        updated_password = "updated_password"

        updated_user.update(
            name=updated_name,
            email=updated_email,
            password=updated_password
        )

        assert updated_name == updated_user.name
        assert updated_email == updated_user.email
        assert user_id == updated_user.id
        assert user_password_hash != updated_user.password_hash

    def test_user_exists(self, db_session_ctx):
        user_controller = UserController()

        email = "test@test.com"

        assert not User.exists(email=email)

        user_controller.create_user(
            name="ted",
            email=email,
            password="password"
        )

        assert User.exists(email=email)

    def test_delete_user(self, db_session_ctx):
        user_controller = UserController()

        user = user_controller.create_user(
            name="user_name",
            email="user_email@email.com",
            password="user_password"
        )

        assert user in user_controller.all_users()
        user.delete()
        assert user not in user_controller.all_users()
