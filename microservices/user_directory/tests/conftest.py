import pytest

from context_helper.context import Context
from objectql_datarm.db import DatabaseManager


@pytest.fixture(scope='session')
def db_manager():
    db_manager = DatabaseManager()

    db_manager.install()

    return db_manager


@pytest.fixture
def db_session_ctx(db_manager):
    db_session = db_manager.db.session()

    try:
        ctx = Context(db_session=db_session)
        ctx.push()
        yield ctx
        ctx.pop()

    except Exception:
        db_session.rollback()
        raise
    else:
        db_session.rollback()
    finally:
        db_session.close()
