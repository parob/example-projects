from typing import Optional

from objectql.decorators import mutation, query
from objectql_datarm.db import ModelBase

from user_directory.controllers.crypt import CryptController

from dataclasses import dataclass
from uuid import UUID

from datarm.filter import Filter


@dataclass
class UserFilter(Filter):
    """
    The `UserFilterInput` ObjectType represents a set of attributes for
    filtering an `User` search.

    Attributes:
        id: The uuid for the `User`.
        email: A case-insensitive filter on the email of the `User`.
    """
    id: UUID = None
    email: str = None

    def criterion(self, entities=None):
        criterion = super().criterion(entities=entities)

        if self.id:
            criterion.append(User.id == self.id)

        if self.email:
            criterion.append(User.email == self.email)

        return criterion


class User(ModelBase):
    """
    The `User` ObjectType represents a email based user account.
    """
    name: Optional[str] = None
    email: str = None
    password_hash: bytes = None

    # noinspection PyShadowingBuiltins
    @classmethod
    def get(cls, id: UUID = None, email: str = None):
        if id:
            return super().get(id=id)

        if email:
            return cls.filter(email=email).one_or_none()

    @classmethod
    def exists(cls, email: str):
        return cls.filter(email=email).one_or_none() is not None

    def __init__(self, email, password=None, name=None):
        if User.exists(email=email):
            raise NameError(f"User with email '{email}' already exists.")

        if email.count("@") != 1 or "." not in email.split("@")[1]:
            raise ValueError(f"Email {email} is invalid.")

        super().__init__()
        self.email = email
        self.password = password
        self.name = name

    @property
    def password(self):
        raise AttributeError(
            "The password is not stored directly, it can only be set."
        )

    @password.setter
    def password(self, plaintext_password):
        if plaintext_password:
            self.password_hash = CryptController.hash(plaintext_password)

    @query
    def check_password(self, password: str) -> bool:
        """
        Checks a password is valid for a given `User`.
        The password should be a plaintext password string.
        """
        return CryptController.check_hash(password, self.password_hash)

    @mutation
    def update(
        self,
        email: str = None,
        password: str = None,
        name: str = None
    ) -> 'User':
        """
        Updates a `User`
        """

        if email:
            self.email = email

        if password:
            self.password = password

        if name:
            self.name = name

        return self
