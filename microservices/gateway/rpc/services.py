import os

from gateway.rpc.access_control import AccessControlService
from gateway.rpc.project_management import ProjectManagementService
from gateway.rpc.user_directory import UserDirectoryService
from service_controller import RemoteServicePool, RemoteService

service_pool = RemoteServicePool(
    name="gateway",
    version="1.0.0",
    services=[
        RemoteService(
            name="user_directory",
            url=os.environ.get(
                "USER_DIRECTORY_SERVICE_URL",
                "http://localhost:5000/user_directory"
            ),
            version="1.0.0",
            stub=UserDirectoryService,
        ),
        RemoteService(
            name="access_control",
            url=os.environ.get(
                "ACCESS_CONTROL_SERVICE_URL",
                "http://localhost:5000/access_control"
            ),
            version="1.0.0",
            stub=AccessControlService,
        ),
        RemoteService(
            name="project_management",
            url=os.environ.get(
                "PROJECT_MANAGEMENT_SERVICE_URL",
                "http://localhost:5000/project_management"
            ),
            version="1.0.0",
            stub=ProjectManagementService,
        )
    ]
)
