from typing import Optional, List
from uuid import UUID

from objectql import query, mutation

from gateway.models.resource import RelationshipType, Relationship, Permission


class AccessControlService:

    @query
    def permission(
        self,
        start_node: UUID,
        end_node: UUID,
        activity: str,
        max_length: int = 6
    ) -> Optional[Permission]:
        """ Remote invocation """
        pass

    @query
    def all_permissions(
        self,
        start_node: UUID = None,
        activity: str = None,
        end_labels: List[str] = None,
        max_length: int = 6
    ) -> List[Permission]:
        """ Remote invocation """
        pass

    @mutation
    def create_relationship(
        self,
        start_node: UUID,
        end_node: UUID,
        activity: str,
        start_labels: List[str] = None,
        end_labels: List[str] = None,
        relationship_type: RelationshipType = RelationshipType.restricted,
        authority: UUID = None
    ) -> Relationship:
        """ Remote invocation """
        pass

    @query
    def relationship(
        self,
        start_node: UUID,
        end_node: UUID,
        activity: str,
        relationship_type: RelationshipType = None,
        authority: UUID = None
    ) -> Optional[Relationship]:
        """ Remote invocation """
        pass
