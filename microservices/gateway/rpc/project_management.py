from typing import Optional, List

from objectql import mutation, query

from gateway.models.project import ProjectStage, Project, ProjectFilter
from gateway.models.utils import OrderBy


class ProjectManagementService:
    """
    The Project Management service provides project creation, getting projects,
    and updating projects.
    """

    @mutation
    def create_project(self, name: str, stage: ProjectStage) -> Project:
        """ Remote invocation """
        pass

    @query
    def project(self, filter: ProjectFilter) -> Optional[Project]:
        """ Remote invocation """
        pass

    @query
    def all_projects(
        self,
        limit: int = 50,
        offset: int = 0,
        order_by: List[OrderBy] = None,
        filter: ProjectFilter = None
    ) -> List[Project]:
        """ Remote invocation """
        pass
