from typing import Optional, List

from objectql import mutation, query

from gateway.models.user import UserFilter, User as BaseUser
from gateway.models.utils import OrderBy


class User(BaseUser):
    """
    The `User` ObjectType represents a remote based user account.
    """
    @classmethod
    def get_labels(cls) -> List[str]:
        return ['User']

    @query
    def check_password(self, password: str) -> bool:
        """ Remote invocation """
        pass

    @mutation
    def update(
        self,
        email: str = None,
        password: str = None,
        name: str = None
    ) -> 'User':
        """ Remote invocation """
        pass


class UserDirectoryService:
    """
    The user service provides user creation, getting users,
    checking user passwords and updating users.
    """

    @mutation
    def create_user(self, name: str, email: str, password: str) -> User:
        """ Remote invocation """
        pass

    @query
    def user(self, filter: UserFilter) -> Optional[User]:
        """ Remote invocation """
        pass

    @query
    def all_users(
        self,
        limit: int = 50,
        offset: int = 0,
        order_by: List[OrderBy] = None,
        filter: UserFilter = None
    ) -> List[User]:
        """ Remote invocation """
        pass
