from context_helper.context import ctx

from gateway.api import api
from gateway.controllers.current_user import SIGN_IN_STATE, RequiredSignInState, CurrentUserController
from gateway.models.user import UserFilter
from gateway.rpc.user_directory import User


@api.object
class AuthController:
    """
    `AuthController` handles login, logout
    """

    @api.mutation({SIGN_IN_STATE: RequiredSignInState.SIGNED_OUT})
    def sign_up(
        self,
        name: str,
        email: str,
        password: str,
        receive_emails: bool = False
    ) -> User:
        """
        Register a new `User`, you must be signed out in order to sign up
        """

        # Create a new user in the remote service
        remote_user = ctx.user_directory_service.create_user(
            email=email,
            password=password,
            name=name
        )

        # Set the current_user so that the user is now logged in
        CurrentUserController().set_current_user(remote_user)

        # 'remote_user' should be a 'RemoteUser' instance.
        # 'RemoteUser' is a subclass of 'User', so we can directly return it.
        return remote_user

    @api.mutation({SIGN_IN_STATE: RequiredSignInState.SIGNED_OUT})
    def sign_in(self, email: str, password: str) -> User:
        """
        Sign in to an existing `User`, you must
        be signed out in order to sign in
        """
        if ctx.current_user:
            raise AttributeError(f"Already logged in as {ctx.current_user}")

        user = ctx.user_directory_service.user(
            filter=UserFilter(email=email)
        )

        if user and user.check_password(password):
            CurrentUserController().set_current_user(user)
            return user

        else:
            raise PermissionError("Incorrect username/password")

    @api.mutation
    def sign_out(self) -> bool:
        """
        Sign out from the current `User`
        """
        CurrentUserController().clear_current_user()
        return True

    @api.query({SIGN_IN_STATE: RequiredSignInState.ANY})
    def is_signed_in(self) -> bool:
        """
        Check if the current `User` is signed in
        """
        return bool(ctx.current_user)

    @api.mutation
    def update_password(
        self,
        current_password: str,
        new_password: str
    ) -> bool:
        """
        Update the current `Users` password
        """
        if not ctx.current_user.check_password(current_password):
            raise AttributeError("Incorrect current password")

        ctx.current_user.update(password=new_password)
        return True
