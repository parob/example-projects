from context_helper import ctx

from gateway.api import api
from gateway.controllers.auth import AuthController
from gateway.controllers.project import ProjectController
from gateway.controllers.user import \
    UserController
from gateway.controllers.current_user import SIGN_IN_STATE, RequiredSignInState
from gateway.rpc.user_directory import User


@api.root
class RootController:
    """
    `RootController` is the main entry point for the GraphQL API Gateway
    """

    @api.query({SIGN_IN_STATE: RequiredSignInState.ANY})
    def auth(self) -> AuthController:
        """
        `AuthController` handles login, logout, reset password, finding users
        """
        return AuthController()

    @api.query
    def user(self) -> UserController:
        """
        `UserController` handles finding users
        """
        return UserController()

    @api.query
    def me(self) -> User:
        """
        Get the current `User`
        """
        return ctx.current_user

    @api.query
    def project(self) -> ProjectController:
        """
        `ProjectController` handles project management
        """
        return ProjectController()
