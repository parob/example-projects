from uuid import UUID

from context_helper.context import ctx

from gateway.api import api
from gateway.models.user import UserFilter
from gateway.rpc.user_directory import User


@api.object
class UserController:
    """
    `UserController` handles finding users
    """

    # noinspection PyShadowingBuiltins
    @api.query
    def user(self, id: UUID = None, email: str = None) -> User:
        """
        Find a `User` with the `Users` id or email
        """
        if id:
            user_filter = UserFilter(id=id)
        else:
            user_filter = UserFilter(email=email)

        return ctx.user_directory_service.user(filter=user_filter)
