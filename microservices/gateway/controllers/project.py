from typing import List
from uuid import UUID

from context_helper.context import ctx

from gateway.api import api
from gateway.models.project import \
    Project, \
    ProjectFilter # , \
    #ProjectActivity


@api.object
class ProjectController:
    """
    Project Controller
    """

    @api.mutation
    def create_project(self, name: str) -> Project:
        """
        Create a new `Project`
        """
        project = ctx.project_management_service.create_project(
            name=name
        )

        # if ctx.current_user:
        #     ctx.current_user.create_relationship(project, [
        #         ProjectActivity.READ,
        #         ProjectActivity.WRITE
        #     ])

        return project

    @api.query
    def project(self, id: UUID) -> Project:
        """
        Find a `Project` with the `Project` id
        """
        return ctx.project_management_service.project(
            filter=ProjectFilter(id=id)
        )

    @api.query
    def all_projects(self, filter: ProjectFilter = None) -> List[Project]:
        """
        Find all `Projects` with the specified filter
        """
        if filter:
            return ctx.project_management_service.all_projects(filter=filter)

        return ctx.project_management_service.all_projects()
