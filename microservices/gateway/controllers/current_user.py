import enum
import os

from typing import Optional, Callable

# noinspection PyDeprecation
from werkzeug.contrib.securecookie import SecureCookie
from werkzeug import Request, Response
from context_helper import Context, ctx, ContextNotAvailable

from gateway.controllers.user import UserController
from gateway.rpc.user_directory import User

SIGN_IN_STATE = "sign_in_state"
CURRENT_USER_REQUIRED = "current_user_required"
CTX_KEY = "current_user"


class RequiredSignInState(enum.Enum):
    ANY = "any"
    SIGNED_IN = "signed_in"
    SIGNED_OUT = "signed_out"


class CurrentUserController:
    """
    Provides a current user context, and persists
    the current user between requests (in the same session) using cookies.

    Provides GraphQL middleware to restrict a GraphQL Node to one of
    the above 'RequiredSignInState's (specified by the Nodes metadata).
    """

    @staticmethod
    def get_session(request: Request) -> SecureCookie:
        if hasattr(request, 'session'):
            return request.session
        else:
            request.session = SecureCookie.load_cookie(
                request,
                secret_key=os.environ.get('SESSION_SECRET_KEY', 'secret_key')
            )
            return request.session

    @staticmethod
    def load_user(request: Request) -> Optional[User]:
        """
        Find a user id based on a request
        """
        session = CurrentUserController.get_session(request=request)

        if 'user_id' in session:
            return UserController().user(id=session['user_id'])

    @staticmethod
    def save_user(
        user: User,
        request: Request,
        response: Response
    ) -> Response:

        session = CurrentUserController.get_session(request=request)

        if user and hasattr(user, 'id'):
            session['user_id'] = user.id
            session.save_cookie(response)

        elif 'user_id' in session:
            del session['user_id']
            session.save_cookie(response)

        return response

    @staticmethod
    def with_current_user(
        func: Callable = None
    ):
        """
        Get the current_user, then wrap `func`
        in a new context so it can access the current_user.
        """

        def with_context(request: Request):
            ctx_dict = {
                CTX_KEY: None
            }
            with Context(**ctx_dict):
                user = CurrentUserController.load_user(request=request)

                CurrentUserController.set_current_user(user)

                try:
                    response = func(request=request)
                except Exception:
                    raise
                else:
                    CurrentUserController.save_user(
                        user=CurrentUserController.current_user(),
                        request=request,
                        response=response
                    )
                finally:
                    CurrentUserController.clear_current_user()

                return response

        return with_context

    @staticmethod
    def graphql_current_user_required_middleware(next_, context):
        """
        GraphQL middleware,
        by default every field expects a user to be logged in.
        """
        if context.request.info.field_name != '__typename':
            required_login_state = context.field.meta.get(
                SIGN_IN_STATE,
                RequiredSignInState.SIGNED_IN
            )

            if required_login_state != RequiredSignInState.ANY:
                _current_user = CurrentUserController.current_user()

                if required_login_state == RequiredSignInState.SIGNED_IN:
                    if _current_user is None:
                        raise PermissionError(
                            f"You must be logged in to access "
                            f"'{context.request.info.field_name}'."
                        )

                    elif not hasattr(_current_user, 'id'):
                        raise AttributeError(
                            f"Invalid current user '{_current_user}', "
                            f"expecting a object with an 'id'."
                        )

                elif required_login_state == RequiredSignInState.SIGNED_OUT:
                    if _current_user is not None:
                        raise AttributeError(
                            f"Logged in as '{_current_user}', "
                            f"You must be logged out to access "
                            f"'{context.request.info.field_name}'."
                        )

        return next_()

    @staticmethod
    def current_user() -> Optional[User]:
        """
        The current `User`
        """
        try:
            return getattr(ctx, CTX_KEY)
        except (ContextNotAvailable, AttributeError):
            return None

    @staticmethod
    def set_current_user(user: Optional[User]):
        """
        Set the current `User`
        """
        setattr(ctx, CTX_KEY, user)
        ctx.current_user = user

    @staticmethod
    def clear_current_user():
        """
        Clear the current `User`
        """
        setattr(ctx, CTX_KEY, None)
