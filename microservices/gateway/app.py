import os

from werkzeug.wrappers import Request
from graphql_http_server import GraphQLHTTPServer

from gateway.api import api
from gateway.controllers.current_user import CurrentUserController
from gateway.controllers.root import RootController     # noqa: F401
from gateway.rpc.services import service_pool

middleware = [CurrentUserController.graphql_current_user_required_middleware]

query_fp = os.path.dirname(os.path.realpath(__file__)) + '/default.graphql'
with open(query_fp, 'r') as query:
    default_query = query.read()

server = GraphQLHTTPServer.from_schema(
    schema=api,
    middleware=middleware,
    allow_cors=True,
    graphiql_default_query=default_query
)


@service_pool.with_services
@CurrentUserController.with_current_user
def main(request: Request):
    return server.dispatch(request=request)


app = server.app(main=main)
