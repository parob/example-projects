import enum

from dataclasses import dataclass
from typing import List
from uuid import UUID

from context_helper import ctx

from gateway.api import api
from gateway.models.resource import Resource, Activity
from gateway.models.utils import Filter


class ProjectStage(enum.Enum):
    """
    The `ProjectStage` EnumType represents the stage of a `Project`
    """
    STAGE_1 = "stage_1"
    STAGE_2 = "stage_2"
    STAGE_3 = "stage_3"


@dataclass
class ProjectFilter(Filter):
    """
    The `ProjectFilterInput` ObjectType represents a set of attributes for
    filtering an `Project` search.

    Attributes:
    id: The uuid for the `Project`.
    name: A case-insensitive filter on the name of the `Project`.
    """
    id: UUID = None
    name: str = None
    stage: ProjectStage = None


class ProjectActivity(Activity):
    """
    The `ProjectActivity` EnumType represents the activities
    that can be performed on a `Project`

    """
    READ = "read"
    WRITE = "write"


@dataclass
class Project(Resource):
    """
    The `Project` ObjectType represents a Project
    """
    name: str = None
    stage: ProjectStage = None

    @classmethod
    def get_labels(cls) -> List[str]:
        return ['Project']

    @api.mutation
    def delete(self) -> bool:
        """
        Delete this `Project`
        """
        return ctx.project_management_service.delete_project(id=self.id)

    @api.mutation
    def update(
        self,
        name: str = None,
        stage: ProjectStage = None
    ) -> 'Project':
        """
        Update this `Project`.
        """
        return ctx.project_management_service.update_project(
            id=self.id,
            name=name,
            stage=stage
        )
