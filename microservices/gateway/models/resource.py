import enum
from dataclasses import dataclass
from typing import List, Union, Optional
from uuid import UUID

from context_helper.context import ctx
from objectql.relay import Node


class Activity(enum.Enum):
    pass


class RelationshipType(enum.Enum):
    restricted = "restricted"
    unrestricted = "unrestricted"


@dataclass
class Relationship:
    """
    A `Relationship` links together two `Resources` with an `Activity`

    `Relationships` are directional e.g.
        'start_node' -> 'activity' -> 'end_node'

    The 'authority' is the 'node' that created the `Relationship`.
    """
    start_node: UUID = None
    end_node: UUID = None
    activity: Activity = None
    relationship_type: RelationshipType = None
    authority: Optional[UUID] = None


@dataclass
class Permission:
    """
    A `Permission` defines the ability for
    'start_node' to perform 'activity' on 'end_node'.

    Each `Permission` is either a single
    `Relationship` or a chain of `Relationships`.

    If a `Relationship` is 'restricted' it cannot be
    used in a `Relationship` chain.
    """
    start_node: UUID = None
    end_node: UUID = None
    activity: Activity = None
    chain: List[Relationship] = None


@dataclass
class Resource(Node):
    """
    A `Resource` is a interface.

    Any model that can have permissions should
    subclass `Resource`.
    """
    id: UUID
    labels: List[str] = None

    @classmethod
    def graphql_exclude_fields(cls):
        return ["labels"]

    def get_labels(self) -> List[str]:
        if self.labels is not None:
            return self.labels

        return [self.__class__.__name__]

    def create_relationship(
        self,
        to_resource: 'Resource',
        activities: List[Activity],
        authority: 'Resource' = None,
        relationship_type: RelationshipType = RelationshipType.restricted
    ):

        for activity in activities:
            ctx.access_control_service.create_relationship(
                start_node=self.id,
                start_labels=self.get_labels(),
                end_node=to_resource.id,
                end_labels=to_resource.get_labels(),
                activity=activity.value,
                authority=authority,
                relationship_type=relationship_type
            )

    def check_permission(
        self,
        to_resource: Union['Resource', UUID],
        activity: Activity,
        max_length: int = 6,
        raise_error: bool = True
    ) -> bool:
        if hasattr(to_resource, 'id'):
            end_resource_id = to_resource.id

        elif isinstance(to_resource, UUID):
            end_resource_id = to_resource
        else:
            raise TypeError(f"Cannot check permission to {to_resource}")

        permission = ctx.access_control_service.permission(
            start_node=self.id,
            end_node=end_resource_id,
            activity=activity.value,
            max_length=max_length
        )

        if permission:
            return True

        if raise_error:
            raise PermissionError(
                f"Permission denied for '{self}' "
                f"to '{activity.value}' '{end_resource_id}'"
            )

        return False
