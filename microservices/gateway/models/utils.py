from dataclasses import dataclass
from enum import Enum
from typing import List

from gateway.api import api


class OrderByDirection(Enum):
    asc = "asc"
    desc = "desc"


@api.object
@dataclass
class OrderBy:
    key: str
    direction: OrderByDirection = OrderByDirection.asc


@api.object
@dataclass
class Filter:
    and_: List['Filter'] = None
    or_: List['Filter'] = None
