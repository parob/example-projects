from dataclasses import dataclass
from typing import Optional

from uuid import UUID

from gateway.api import api
from gateway.models.utils import Filter
from gateway.models.resource import Resource


@api.object
@dataclass
class UserFilter(Filter):
    """
    The `UserFilter` ObjectType can be used to filter a
    list of user accounts.
    """
    id: UUID = None
    email: str = None


@api.object
@dataclass
class User(Resource):
    """
    The `User` ObjectType represents a email based user
    account.
    """
    name: Optional[str] = None
    email: str = None
