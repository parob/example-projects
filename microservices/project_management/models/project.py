import enum
from typing import Optional

from objectql.decorators import mutation
from objectql_datarm.db import ModelBase

from dataclasses import dataclass
from uuid import UUID

from datarm.filter import Filter


class ProjectStage(enum.Enum):
    """
    The `ProjectStage` EnumType represents the stage of a `Project`
    """
    STAGE_1 = "stage_1"
    STAGE_2 = "stage_2"
    STAGE_3 = "stage_3"


@dataclass
class ProjectFilter(Filter):
    """
    The `ProjectFilterInput` ObjectType represents a set of attributes for
    filtering an `Project` search.

    Attributes:
    id: The uuid for the `Project`.
    name: A case-insensitive filter on the name of the `Project`.
    stage: A filter on the stage of th `Project`.
    """
    id: UUID = None
    name: str = None
    stage: ProjectStage = None

    def criterion(self, entities=None):
        criterion = super().criterion(entities=entities)

        if self.id:
            criterion.append(Project.id == self.id)

        if self.name:
            criterion.append(Project.name == self.name)

        if self.stage:
            criterion.append(Project.stage == self.stage)

        return criterion


class Project(ModelBase):
    """
    The `Project` ObjectType represents a project.
    """
    name: Optional[str] = None
    stage: ProjectStage = ProjectStage.STAGE_1

    @classmethod
    def exists(cls, name: str):
        return cls.filter(name=name).one_or_none() is not None

    def __init__(
        self,
        name=None,
        stage: ProjectStage = ProjectStage.STAGE_1
    ):
        if Project.exists(name=name):
            raise NameError(f"Project with name '{name}' already exists.")

        super().__init__()
        self.name = name
        self.stage = stage

    @mutation
    def update(
        self,
        name: str = None,
        stage: ProjectStage = None
    ) -> 'Project':
        """
        Updates a `Project`
        """
        if name:
            self.name = name

        if stage:
            self.stage = stage

        return self
