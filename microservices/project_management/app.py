from objectql.schema import ObjectQLSchema
from graphql_http_server import GraphQLHTTPServer

from werkzeug.wrappers import Request

from objectql_datarm.db import DatabaseManager

from service_controller import RemoteServicePool
from project_management.controllers.project import ProjectController

db_manager = DatabaseManager()

server = GraphQLHTTPServer.from_schema(
    schema=ObjectQLSchema(root=ProjectController)
)

service_controller = RemoteServicePool(
    name="project_management",
    version="1.0.0"
)


@service_controller.with_services
@db_manager.with_db_session
def main(request: Request):
    return server.dispatch(request=request)


app = server.app(main=main)
