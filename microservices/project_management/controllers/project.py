from typing import List, Optional

from datarm.order_by import OrderBy, OrderByDirection
from objectql import query, mutation

from project_management.models.project import \
    Project, \
    ProjectFilter, \
    ProjectStage


class ProjectController:
    """
    Project management controller
    """

    @mutation
    def create_project(
        self,
        name: str,
        stage: ProjectStage = ProjectStage.STAGE_1
    ) -> Project:
        """
        Creates a new `Project`
        """
        project = Project(name=name, stage=stage)
        project.create()

        return project

    @query
    def project(self, filter: ProjectFilter) -> Optional[Project]:
        """
        Finds a single `Project` with a filter.
        """
        return Project.query().filter(filter).one_or_none()

    @query
    def all_projects(
        self,
        limit: int = 50,
        offset: int = 0,
        order_by: List[OrderBy] = None,
        filter: ProjectFilter = None
    ) -> List[Project]:
        """
        Finds all `Projects` with a filter.

        :param limit: The maximum number of `Projects` that should be returned.
        :param offset: The offset, return `Projects` after the nth (offset)
        `Projects`
        :param order_by:
        :param filter:
        :return:
        """
        if order_by is None:
            # noinspection PyArgumentList
            order_by = [OrderBy(key='name', direction=OrderByDirection.asc)]

        return Project \
            .filter(filter) \
            .order_by(*order_by) \
            .offset(offset) \
            .limit(limit) \
            .all()
