from typing import Optional
from typing import List
from uuid import UUID

from context_helper.context import ctx

from objectql import mutation, query

from access_control.models.permission import Permission
from access_control.models.relationship import \
    RelationshipType, \
    Relationship, \
    ACTIVITY_WILDCARD


class AccessControl:

    @query
    def permission(
        self,
        start_node: UUID,
        end_node: UUID,
        activity: str,
        max_length: int = 6,
        session=None
    ) -> Optional[Permission]:

        if session is None:
            session = ctx.db_session

        if start_node == end_node:
            return Permission(
                start_node=start_node,
                end_node=end_node,
                activity=activity
            )

        unrestricted = RelationshipType.unrestricted.value

        match = "(a:Resource {id: {start_id}}), (b:Resource {id: {end_id}})"

        where = f"(type(r)={{activity}} OR type(r)={{activity_wildcard}}) " \
                f"  AND " \
                f"(" \
                f"  r.relationship_type='{unrestricted}'" \
                f"      OR " \
                f"  endNode(r)=b" \
                f")"

        cypher = f"MATCH {match}" \
                 f" MATCH p=shortestPath((a)-[*1..{max_length}]->(b)) " \
                 f"      WHERE ALL(r IN relationships(p) " \
                 f"          WHERE {where}" \
                 f"      ) " \
                 f"RETURN p"

        result = session.run(cypher, {
            'start_id': str(start_node),
            'end_id': str(end_node),
            'activity': activity,
            'activity_wildcard': ACTIVITY_WILDCARD
        })

        record = result.single()

        if not record:
            return

        chain = record[0]

        return Permission(activity=activity, chain=chain)

    @query
    def all_permissions(
        self,
        start_node: UUID = None,
        activity: str = None,
        end_labels: List[str] = None,
        max_length: int = 6,
        session=None
    ) -> List[Permission]:

        if session is None:
            session = ctx.db_session

        if end_labels:
            end_labels_clause = ":" + ":".join(end_labels)
        else:
            end_labels_clause = ""

        if session is None:
            session = ctx.db_session

        unrestricted = RelationshipType.unrestricted.value

        match = f"(a:Resource {{id: {{start_id}}}}), " \
                f"(b:Resource{end_labels_clause})"

        where = f"(type(r)={{activity}} OR type(r)={{activity_wildcard}}) " \
                f"  AND " \
                f"  (" \
                f"      r.relationship_type='{unrestricted}' " \
                f"          OR " \
                f"      endNode(r)=b" \
                f"  )"

        cypher = f"MATCH {match}" \
                 f"MATCH p=(a)-[*1..{max_length}]->(b) " \
                 f"WITH p, b " \
                 f"      WHERE ALL(r IN relationships(p) " \
                 f"          WHERE {where}" \
                 f"      ) " \
                 f"RETURN p, b"

        result = session.run(cypher, {
            'start_id': str(start_node),
            'activity': activity,
            'activity_wildcard': ACTIVITY_WILDCARD
        })

        records = {record for record in result}

        def to_id(relationship):
            if relationship.get('id'):
                return UUID(relationship['id'])

        return [
            Permission(
                start_node=start_node,
                end_node=UUID(node['id']),   # unused: node.labels
                activity=activity,
                chain=[
                    Relationship(
                        start_node=relationship.start_node,
                        end_node=relationship.end_node,
                        activity=relationship.type,
                        authority=to_id(relationship),
                        relationship_type=RelationshipType(
                            relationship['relationship_type']
                        )
                    )
                    for relationship in chain.relationships
                ]
            )
            for chain, node in records
        ]

    @mutation
    def create_relationship(
        self,
        start_node: UUID,
        end_node: UUID,
        activity: str,
        start_labels: List[str] = None,
        end_labels: List[str] = None,
        relationship_type: RelationshipType = RelationshipType.restricted,
        authority: UUID = None,
        session=None
    ) -> Relationship:

        if session is None:
            session = ctx.db_session

        if not start_labels:
            start_labels = []

        if not end_labels:
            end_labels = []

        start_labels = ":".join({'Resource', *start_labels})
        end_labels = ":".join({'Resource', *end_labels})

        meta = f"{{ " \
               f"   relationship_type: {{relationship_type}}," \
               f"   authority: {{authority}} " \
               f"}}"

        cypher = f"MERGE (a:{start_labels} {{id: {{start_id}}}})" \
                 f"MERGE (b:{end_labels} {{id: {{end_id}}}})" \
                 f"MERGE (a)-[r:{activity} {meta}]->(b)" \
                 f"RETURN r"

        result = session.run(cypher, {
            'start_id': str(start_node),
            'end_id': str(end_node),
            'relationship_type': relationship_type.value,
            'authority': str(authority) if authority else ""
        })

        record = result.single()

        if not record or not record[0]:
            raise ValueError(
                "Relationship create did not error, "
                "but expected relationship record was not returned."
            )

        return Relationship(
            start_node=start_node,
            end_node=end_node,
            activity=activity,
            relationship_type=relationship_type,
            authority=authority
        )

    @query
    def relationship(
        self,
        start_node: UUID,
        end_node: UUID,
        activity: str,
        relationship_type: RelationshipType = None,
        authority: UUID = None,
        session=None
    ) -> Optional[Relationship]:

        if session is None:
            session = ctx.db_session

        if start_node == end_node:
            return Relationship(
                start_node=start_node,
                end_node=end_node,
                activity=activity
            )

        relation = f"[r:{activity}|:{ACTIVITY_WILDCARD}]"

        data = {
            'start_id': str(start_node),
            'end_id': str(end_node)
        }

        where_relationship_clauses = []

        if relationship_type:
            where_relationship_clauses.append(
                "r.relationship_type='{relationship_type}'"
            )
            data['relationship_type'] = relationship_type.value

        if authority:
            where_relationship_clauses.append("r.authority='{authority}'")
            data['authority'] = str(authority)

        where_clause = ""

        if where_relationship_clauses:
            where_clause = f"WHERE {' OR '.join(where_relationship_clauses)}"

        match_clause = f"(a:Resource {{id: {{start_id}}}})" \
                       f"-{relation}->" \
                       f"(b:Resource {{id: {{end_id}}}})"

        cypher = f"MATCH  {match_clause}" \
                 f"{where_clause}" \
                 f"RETURN r"

        result = session.run(cypher, data)
        record = result.single()

        if not record or not record[0]:
            return None

        return Relationship(
            start_node=start_node,
            end_node=end_node,
            activity=activity,
            relationship_type=relationship_type,
            authority=authority
        )
