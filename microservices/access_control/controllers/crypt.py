import uuid
import bcrypt
import jwt

from typing import Dict


class CryptController:

    @staticmethod
    def hash(value: str) -> bytes:
        return bcrypt.hashpw(value.encode("utf-8"), CryptController.salt())

    @staticmethod
    def salt() -> bytes:
        return bcrypt.gensalt()

    @staticmethod
    def check_hash(value: str, hash: bytes):
        if value:
            return bcrypt.checkpw(value.encode('utf-8'), hash)

    def __init__(self, secret=None, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if secret is None:
            secret = str(uuid.uuid4())

        self.secret = secret

    def encode(self, value: Dict) -> bytes:
        return jwt.encode(value, self.secret, algorithm='HS256')

    def decode(self, encoded_value: bytes) -> Dict:
        return jwt.decode(encoded_value, self.secret, algorithms=['HS256'])
