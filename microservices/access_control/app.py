import os

from objectql.schema import ObjectQLSchema
from graphql_http_server import GraphQLHTTPServer
from werkzeug.wrappers import Request

from access_control.db import Neo4JDatabaseManager

from access_control.controllers.access import AccessControl
from service_controller import RemoteServicePool

db_manager = Neo4JDatabaseManager(
    url=os.environ.get("NEO4J_URL", "bolt://localhost:7687")
)

server = GraphQLHTTPServer.from_schema(schema=ObjectQLSchema(root=AccessControl))

service_controller = RemoteServicePool(
    name="access_control",
    version="1.0.0"
)


@service_controller.with_services
@db_manager.with_db_session
def main(request: Request):
    return server.dispatch(request=request)


app = server.app(main=main)
