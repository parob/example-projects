from dataclasses import dataclass
from typing import List
from uuid import UUID

from access_control.models.relationship import Relationship


@dataclass
class Permission:
    start_node: UUID = None
    end_node: UUID = None
    activity: str = None
    chain: List[Relationship] = None
