import enum

from dataclasses import dataclass
from typing import Optional
from uuid import UUID

from context_helper.context import ctx
from objectql import mutation

ACTIVITY_WILDCARD = "any"


class RelationshipType(enum.Enum):
    restricted = "restricted"
    unrestricted = "unrestricted"


@dataclass
class Relationship:
    start_node: UUID = None
    end_node: UUID = None
    activity: str = None
    relationship_type: RelationshipType = None
    authority: Optional[UUID] = None

    @mutation
    def delete(self, session=None) -> bool:
        if session is None:
            session = ctx.db_session

        cypher = f"MATCH " \
            f"(a:Resource {{ id: {{start_id}} }})" \
            f"-[r:{self.activity}]-" \
            f"(b:Resource {{ id: {{end_id}} }}) " \
            f"DELETE r"

        return session.run(cypher, {
            'start_id': str(self.start_node),
            'end_id': str(self.end_node)
        })
