import pytest

from context_helper.context import Context

from access_control.app import db_manager as _db_manager
from access_control.controllers.access import AccessControl
from access_control.db import Neo4JDatabaseManager


@pytest.fixture(scope="session")
def db_manager():
    return _db_manager


@pytest.fixture
def access_control() -> AccessControl:
    return AccessControl()


@pytest.fixture
def db_session(db_manager: Neo4JDatabaseManager):
    session = db_manager.db.session()
    session.begin_transaction()
    yield session

    # noinspection PyProtectedMember
    if session._transaction:
        session.rollback_transaction()


@pytest.fixture
def db_ctx(db_session):
    return Context(clone=False, db_session=db_session)
