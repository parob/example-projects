import uuid


from access_control.models.relationship import \
    ACTIVITY_WILDCARD, \
    RelationshipType


class TestGBACService:

    def test_no_permission(self, db_ctx, access_control):
        resource_1 = uuid.uuid4()
        resource_2 = uuid.uuid4()

        with db_ctx:
            assert not access_control.permission(
                resource_1,
                resource_2,
                "ACCESS"
            )

    def test_create_permission(self, db_ctx, access_control):
        resource_1 = uuid.uuid4()
        resource_2 = uuid.uuid4()

        with db_ctx:
            access_control.create_relationship(
                start_node=resource_1,
                end_node=resource_2,
                activity="ACCESS"
            )

            assert access_control.permission(
                resource_1,
                resource_2,
                "ACCESS"
            )

    def test_delete_permission(self, db_ctx, access_control):
        resource_1 = uuid.uuid4()
        resource_2 = uuid.uuid4()

        with db_ctx:
            access_control.create_relationship(
                resource_1,
                resource_2,
                "ACCESS"
            )

            assert access_control.permission(
                resource_1,
                resource_2,
                "ACCESS"
            )

            relationship = access_control.relationship(
                resource_1,
                resource_2,
                "ACCESS"
            )
            relationship.delete()

            assert not access_control.permission(
                resource_1,
                resource_2,
                "ACCESS"
            )

    def test_wildcard_permission(self, db_ctx, access_control):
        resource_1 = uuid.uuid4()
        resource_2 = uuid.uuid4()

        with db_ctx:
            access_control.create_relationship(
                resource_1,
                resource_2,
                ACTIVITY_WILDCARD
            )

            assert access_control.permission(
                resource_1,
                resource_2,
                "ACCESS"
            )

    def test_self_permission(self, db_ctx, access_control):
        resource_1 = uuid.uuid4()

        with db_ctx:
            assert access_control.permission(
                resource_1,
                resource_1,
                "ACCESS"
            )

    def test_atomic_permission(self, db_ctx, access_control):
        resource_1 = uuid.uuid4()
        resource_2 = uuid.uuid4()

        with db_ctx:
            access_control.create_relationship(
                resource_1,
                resource_2,
                "ACCESS"
            )

            assert not access_control.permission(
                resource_1,
                resource_2,
                "NOT_ACCESS"
            )

    def test_reverse_permission(self, db_ctx, access_control):
        resource_1 = uuid.uuid4()
        resource_2 = uuid.uuid4()

        with db_ctx:
            access_control.create_relationship(
                resource_1,
                resource_2,
                "ACCESS"
            )

            assert not access_control.permission(
                resource_2,
                resource_1,
                "ACCESS"
            )

    def test_forward_permission(self, db_ctx, access_control):
        resource_1 = uuid.uuid4()
        resource_2 = uuid.uuid4()
        resource_3 = uuid.uuid4()

        with db_ctx:
            access_control.create_relationship(
                resource_1,
                resource_2,
                "ACCESS",
                relationship_type=RelationshipType.unrestricted
            )
            access_control.create_relationship(
                resource_2,
                resource_3,
                "ACCESS"
            )

            assert access_control.permission(
                resource_1,
                resource_3,
                "ACCESS"
            )

    def test_restricted_permission(self, db_ctx, access_control):
        resource_1 = uuid.uuid4()
        resource_2 = uuid.uuid4()
        resource_3 = uuid.uuid4()

        with db_ctx:
            access_control.create_relationship(
                resource_1,
                resource_2,
                "ACCESS"
            )
            access_control.create_relationship(
                resource_2,
                resource_3,
                "ACCESS"
            )

            assert not access_control.permission(
                resource_1,
                resource_3,
                "ACCESS"
            )

    def test_recursion_limit_permission(self, db_ctx, access_control):
        resources = [uuid.uuid4() for _ in range(8)]

        with db_ctx:
            previous = resources[0]
            for resource in resources[1:]:
                access_control.create_relationship(
                    start_node=previous,
                    end_node=resource,
                    activity="ACCESS",
                    relationship_type=RelationshipType.unrestricted
                )

                previous = resource

            assert access_control.permission(
                resources[0],
                resources[6],
                "ACCESS"
            )
            assert not access_control.permission(
                resources[0],
                resources[7],
                "ACCESS"
            )
            assert access_control.permission(
                resources[0],
                resources[7],
                "ACCESS",
                max_length=7
            )

    def test_resource_find(self, db_ctx, access_control):
        resource_1 = uuid.uuid4()
        resource_2 = uuid.uuid4()
        resource_3 = uuid.uuid4()

        with db_ctx:
            access_control.create_relationship(
                start_node=resource_1,
                end_node=resource_2,
                end_labels={"TestResource"},
                activity="ACCESS"
            )

            access_control.create_relationship(
                start_node=resource_1,
                end_node=resource_3,
                end_labels={"TestResource"},
                activity="ACCESS"
            )

            permissions = access_control.all_permissions(
                start_node=resource_1,
                activity="ACCESS",
                end_labels={"TestResource"}
            )

            resources = {permission.end_node for permission in permissions}

            assert resources == {resource_2, resource_3}
