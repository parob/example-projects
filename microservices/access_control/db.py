import logging
from typing import Callable

from neo4j import GraphDatabase

from context_helper import Context


class Neo4JDatabaseManager:

    def __init__(
        self,
        url: str = "bolt://localhost:7687",
        username: str = "neo4j",
        password: str = "password"
    ):
        self.logger = logging.getLogger("gbac")
        self.logger.info(f"Connecting Neo4jDatabaseService with url {url}")

        self.url = url
        self.db = GraphDatabase.driver(url, auth=(username, password))

    def install(self, wipe=True) -> bool:
        """
        Create the models in the db
        """
        db_session = self.db.session()

        if wipe:
            db_session.begin_transaction()
            db_session.run(
                "MATCH (n) DETACH DELETE n"
            )
            db_session.commit_transaction()

        db_session.begin_transaction()
        db_session.run(
            "CREATE CONSTRAINT ON (n:Resource) ASSERT n.id IS UNIQUE"
        )
        db_session.commit_transaction()

        return True

    def with_db_session(
        self,
        func: Callable = None,
        context_key_name="db_session"
    ):
        """
        Create a db session, then wrap `func`
        in a new context so it can access the db session.
        """
        def with_context(*args, **kwargs):
            db_session = self.db.session()

            db_session.begin_transaction()
            try:
                with Context(**{context_key_name: db_session}):
                    response = func(*args, **kwargs)

            except Exception:
                db_session.rollback_transaction()
                raise
            else:
                db_session.commit_transaction()
            finally:
                db_session.close()

            return response

        return with_context
