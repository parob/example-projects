from werkzeug.serving import run_simple
from werkzeug.middleware.dispatcher import DispatcherMiddleware

from gateway.app import app as gateway_app

from user_directory.app import \
    app as user_directory, \
    db_manager as user_directory_db_manager

from access_control.app import \
    app as access_control, \
    db_manager as access_control_db_manager

from project_management.app import \
    app as project_management, \
    db_manager as project_management_db_manager


application = DispatcherMiddleware(
    gateway_app,
    {
        '/user_directory': user_directory,
        '/access_control': access_control,
        '/project_management': project_management
    }
)


if __name__ == '__main__':
    user_directory_db_manager.install()
    access_control_db_manager.install()
    project_management_db_manager.install()

    run_simple(
        hostname='localhost',
        port=5000,
        application=application,
        use_reloader=True,
        use_debugger=True,
        threaded=True
    )
