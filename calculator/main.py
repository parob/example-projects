from graphql_api import GraphQLAPI, field
from graphql_http_server import GraphQLHTTPServer


class Calculator:

    @field
    def add(self, a: float, b: float) -> float:
        return a + b

    @field
    def multiply(self, a: float, b: float) -> float:
        return a * b

    @field
    def divide(self, a: float, b: float) -> float:
        return a / b

server = GraphQLHTTPServer.from_api(api=GraphQLAPI(root=Calculator))

# define main fuction for Google Cloud Function support
def main(request):
    return server.dispatch(request=request)

if __name__ == "__main__":
    server.run(main=main, port=3504)
