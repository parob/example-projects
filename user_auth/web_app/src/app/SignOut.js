import React from "react";
import PropTypes from "prop-types";
import {Redirect} from "react-router-dom";

import {withGraphQL} from "../client";
import {isSignedIn, signOut} from "./api";


class SignOut extends React.PureComponent {

    static propTypes = {
        signOut: PropTypes.func.isRequired,
        isSignedIn: PropTypes.bool.isRequired
    };

    componentDidMount() {
        this.props.signOut()
    }

    render() {
        if (!this.props.isSignedIn) {
            return <Redirect to="/"/>;
        }

        return <p> Signing out... </p>;
    }
}

export default withGraphQL({signOut, isSignedIn})(SignOut);
