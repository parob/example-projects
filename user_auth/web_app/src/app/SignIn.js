import React from "react";
import PropTypes from "prop-types";

import {Link} from "react-router-dom";
import {withStyles} from "@material-ui/core/styles";

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Avatar from '@material-ui/core/Avatar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container'

import {withGraphQL} from "../client";

import {signIn} from "./api";

const styles = theme => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    error: {
        color: theme.palette.error.main
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    }
});

class SignIn extends React.PureComponent {

    static propTypes = {
        classes: PropTypes.object.isRequired,
        error: PropTypes.shape({
            message: PropTypes.string.isRequired,
            email: PropTypes.string,
            password: PropTypes.string
        }),

        onSignIn: PropTypes.func,
        onClose: PropTypes.func
    };

    static contextTypes = {
        router: PropTypes.object
    };

    state = {
        email: "",
        password: ""
    };

    handleEmailChange = (event) => {
        this.setState({error: null, email: event.target.value});
    };

    handlePasswordChange = (event) => {
        this.setState({error: null, password: event.target.value});
    };

    handleSignIn = () => {
        this.props.onSignIn(this.state.email, this.state.password);
    };

    render() {
        let {classes, error} = this.props;
        let errorMessage;

        if (error &&
            !error.hasOwnProperty("email") &&
            !error.hasOwnProperty("password") &&
            error.hasOwnProperty("message") &&
            error.message) {
            errorMessage = <p className={classes.error}> {error.message} </p>;
        }

        return (
            <Container component="main" maxWidth="xs">
                <CssBaseline/>
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon/>
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Sign in
                    </Typography>
                    <form className={classes.form} noValidate>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Email Address"
                            name="email"
                            autoComplete="email"
                            value={this.state.email}
                            helperText={error && error.hasOwnProperty("email") ? error.email : ""}
                            error={error && error.hasOwnProperty("email")}
                            onChange={this.handleEmailChange}
                            autoFocus
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            value={this.state.password}
                            helperText={error && error.hasOwnProperty("password") ? error.password : ""}
                            error={error && error.hasOwnProperty("password")}
                            onChange={this.handlePasswordChange}
                        />
                        {errorMessage}
                        <Button
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onClick={this.handleSignIn}
                        >
                            Sign In
                        </Button>
                        <Grid container justify="flex-end">
                            <Grid item>
                                <Link to="/sign_up">
                                    Don't have an account? Sign Up
                                </Link>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Container>
        );
    }
}

const SignInWithStyles = withStyles(styles)(SignIn);

class SignInContainer extends React.PureComponent {

    static propTypes = {
        signIn: PropTypes.func,
    };

    state = {
        error: null
    };

    handleSignIn = (email, password) => {
        this.props.signIn({email, password}).catch(
            (error) => this.setState({error: error})
        );
    };

    render() {
        return (
            <SignInWithStyles
                error={this.state.error}
                onClose={this.handleClose}
                onSignIn={this.handleSignIn}
            />
        );
    }
}


export default withGraphQL({signIn})(SignInContainer);
