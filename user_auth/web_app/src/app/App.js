import React, {Component} from 'react';
import PropTypes from "prop-types";

import {ApolloProvider} from "react-apollo";
import {Route, BrowserRouter, Redirect, Switch} from "react-router-dom";
import {MuiThemeProvider} from "@material-ui/core/styles";
import {withStyles} from "@material-ui/core";

import theme from "../theme";
import client, {withGraphQL} from "../client";
import {apiVersion, isSignedIn} from "./api";

import SignIn from "./SignIn";
import SignUp from "./SignUp"
import Dashboard from "./Dashboard";
import SignOut from "./SignOut";


const styles = (theme) => ({
    "@global": {
        body: {
            backgroundColor: theme.palette.common.white,
        },
        a: {
            textDecoration: "none",
            color: theme.palette.primary.main
        }
    },
    root: {
        margin: "20px"
    },
    footer: {
        marginTop: "25px",
        fontSize: 12,
        textAlign: "center"
    }
});

class App extends Component {

    static propTypes = {
        apiVersion: PropTypes.string.isRequired,
        isSignedIn: PropTypes.bool.isRequired,
        classes: PropTypes.object.isRequired
    };

    render() {
        let {classes, apiVersion, isSignedIn} = this.props;

        let authRoutes;

        if (!isSignedIn) {
            authRoutes = [
                <Route path="/sign_in" component={SignIn} key="sign_in"/>,
                <Route path="/sign_up" component={SignUp} key="sign_up"/>
            ]
        } else {
            authRoutes = [
                <Route path="/sign_out" component={SignOut} key="sign_out"/>
            ]
        }

        return (
            <div className={classes.root}>
                <Switch>
                    {authRoutes}
                    <Route exact path="/" render={() => (
                        !isSignedIn ? <Redirect to="/sign_in"/> : <Dashboard/>
                    )}/>
                    <Redirect from='*' to='/'/>
                </Switch>
                <div className={classes.footer}>
                    WebApp(v{process.env.REACT_APP_VERSION}) API(v{apiVersion})
                </div>
            </div>
        );
    }
}

const AppWithStyles = withStyles(styles)(App);

const AppWithGraphQL = withGraphQL({apiVersion, isSignedIn})(AppWithStyles);

class AppContainer extends React.Component {

    render = () => {
        return (
            <ApolloProvider client={client}>
                <MuiThemeProvider theme={theme}>
                    <BrowserRouter>
                        <AppWithGraphQL/>
                    </BrowserRouter>
                </MuiThemeProvider>
            </ApolloProvider>
        );
    }

}

export default AppContainer;
