import React from "react";
import PropTypes from "prop-types";

import {Link} from "react-router-dom";
import {withStyles} from "@material-ui/core/styles";

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Avatar from '@material-ui/core/Avatar';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';

import {withGraphQL} from "../client";
import {signUp} from "./api";

const styles = (theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    }
});

class SignUp extends React.PureComponent {

    static propTypes = {
        classes: PropTypes.object.isRequired,
        error: PropTypes.shape({
            message: PropTypes.string.isRequired,
            name: PropTypes.string,
            email: PropTypes.string,
            password: PropTypes.string
        }),

        onSignUp: PropTypes.func,
        onClose: PropTypes.func
    };

    static contextTypes = {
        router: PropTypes.object
    };

    state = {
        firstName: "",
        lastName: "",
        email: "",
        password: "",
        receiveEmails: false
    };

    handleFirstNameChange = (event) => {
        this.setState({error: null, firstName: event.target.value});
    };

    handleLastNameChange = (event) => {
        this.setState({error: null, lastName: event.target.value});
    };

    handleEmailChange = (event) => {
        this.setState({error: null, email: event.target.value});
    };

    handlePasswordChange = (event) => {
        this.setState({error: null, password: event.target.value});
    };

    handleReceiveEmailsChange = (event) => {
        this.setState({error: null, receiveEmails: event.target.checked});
    };

    handleSignUp = () => {
        this.props.onSignUp(
            this.state.firstName + " " + this.state.lastName,
            this.state.email,
            this.state.password,
            this.state.receiveEmails
        );
    };

    render() {
        let {classes, error} = this.props;
        let errorMessage;

        if (error &&
            !error.hasOwnProperty("name") &&
            !error.hasOwnProperty("email") &&
            !error.hasOwnProperty("password") &&
            error.hasOwnProperty("message") &&
            error.message) {
            errorMessage = <p className={classes.error}> {error.message} </p>;
        }

        return (
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Sign up
                    </Typography>
                    <form className={classes.form} noValidate>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    autoComplete="fname"
                                    name="firstName"
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="firstName"
                                    label="First Name"
                                    autoFocus
                                    value={this.state.firstName}
                                    helperText={error && error.hasOwnProperty("firstName") ? error.firstName : ""}
                                    error={error && error.hasOwnProperty("firstName")}
                                    onChange={this.handleFirstNameChange}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="lastName"
                                    label="Last Name"
                                    name="lastName"
                                    autoComplete="lname"
                                    value={this.state.lastName}
                                    helperText={error && error.hasOwnProperty("lastName") ? error.lastName : ""}
                                    error={error && error.hasOwnProperty("lastName")}
                                    onChange={this.handleLastNameChange}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="email"
                                    label="Email Address"
                                    name="email"
                                    autoComplete="email"
                                    value={this.state.email}
                                    helperText={error && error.hasOwnProperty("email") ? error.email : ""}
                                    error={error && error.hasOwnProperty("email")}
                                    onChange={this.handleEmailChange}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    name="password"
                                    label="Password"
                                    type="password"
                                    id="password"
                                    autoComplete="current-password"
                                    value={this.state.password}
                                    helperText={error && error.hasOwnProperty("password") ? error.password : ""}
                                    error={error && error.hasOwnProperty("password")}
                                    onChange={this.handlePasswordChange}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <FormControlLabel
                                    control={<Checkbox value="allowExtraEmails"
                                                       color="primary"
                                                       checked={this.state.receiveEmails}
                                                       onChange={this.handleReceiveEmailsChange}/>}
                                    label="I want to receive marketing promotions and updates via email."
                                />
                            </Grid>
                        </Grid>
                        {errorMessage}
                        <Button
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onClick={this.handleSignUp}
                        >
                            Sign Up
                        </Button>
                        <Grid container justify="flex-end">
                            <Grid item>
                                <Link to="/sign_in">
                                    Already have an account? Sign In
                                </Link>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Container>
        );
    }
}

const SignUpWithStyles = withStyles(styles)(SignUp);

class SignUpContainer extends React.PureComponent {

    static propTypes = {
        signUp: PropTypes.func
    };

    state = {
        error: null
    };

    handleSignUp = (name, email, password, receiveEmails) => {
        this.props.signUp({name, email, password, receiveEmails}).catch(
            (error) => {
                this.setState({error: error})
            }
        );
    };

    render() {
        return (
            <SignUpWithStyles
                error={this.state.error}
                onSignUp={this.handleSignUp}
            />
        );
    }
}

export default withGraphQL({signUp})(SignUpContainer);
