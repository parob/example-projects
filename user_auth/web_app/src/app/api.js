import {
    ApiVersion,
    IsSignedIn,
    SignUp,
    SignIn,
    SignOut,
    Me,
    UpdatePassword
} from "./api.gql";

export const apiVersion = {
    query: ApiVersion,
    adapt: (data) => (data.apiVersion)
};

export const isSignedIn = {
    query: IsSignedIn,
    adapt: (data) => (data.isSignedIn)
};

export const signUp = {
    mutation: SignUp,
    adapt: (data) => (data.signUp.id),
    refetch: [isSignedIn]
};

export const signIn = {
    mutation: SignIn,
    adapt: (data) => (data.signIn.id),
    refetch: [isSignedIn]
};

export const me = {
    query: Me,
    adapt: (data) => (data.me)
};

export const signOut = {
    mutation: SignOut,
    adapt: (data) => (data.signOut),
    refetch: [isSignedIn]
};

export const updatePassword = {
    mutation: UpdatePassword,
    adapt: (data) => (data.updatePassword)
};
