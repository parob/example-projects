import React from "react";
import PropTypes from "prop-types";

import {InMemoryCache} from "apollo-cache-inmemory";
import {ApolloClient} from "apollo-client";
import {HttpLink} from "apollo-link-http";

import {Query, Mutation} from "react-apollo";

export default new ApolloClient({
    cache: new InMemoryCache(),
    link: new HttpLink({
        credentials: "include"
    })
});


PropTypes.graphQL = PropTypes.shape({kind: PropTypes.oneOf(["Document"])});

PropTypes.api = {
    mutation: PropTypes.graphQL,
    query: PropTypes.graphQL,
    adapt: PropTypes.func
};

PropTypes.api.refetch = PropTypes.arrayOf(PropTypes.shape(PropTypes.api));

export const withGraphQL = (apiMap) => {

    for (let apiKey in apiMap) {
        if (apiMap.hasOwnProperty(apiKey)) {
            PropTypes.checkPropTypes(PropTypes.api, apiMap[apiKey], 'API attribute', apiKey);
        }
    }

    return (Component) => {
        let wrappedComponent = Component;

        Object.keys(apiMap).forEach((name) => {
            let api = apiMap[name];

            if (api.hasOwnProperty("query")) {
                wrappedComponent = withQuery(name, api)(wrappedComponent)
            }
            if (api.hasOwnProperty("mutation")) {
                wrappedComponent = withMutation(name, api)(wrappedComponent)
            }
        });

        return wrappedComponent;
    };
};

export const adaptError = (error) => {
    console.log(error);
    if (error.graphQLErrors.length > 0) {
        let simpleError = {
            ...error.graphQLErrors[0].extensions,
            message: error.graphQLErrors[0].message
        };
        console.log(simpleError);
        return Promise.reject(simpleError);
    } else {
        let simpleError = {
            message: String(error)
        };
        return Promise.reject(simpleError);
    }

};

export const withQuery = (propName, query) => {

    return (Component) => {

        class WithQuery extends React.Component {

            render() {
                return (
                    <Query query={query.query}>
                        {({loading, error, data, refetch, networkStatus}) => {
                            if (networkStatus === 4) {
                                return <div>Refetching...</div>;
                            }
                            if (error) {
                                return (
                                    <div>
                                        {error.message}.
                                    </div>
                                );
                            }
                            if (loading) {
                                return <div>Loading...</div>;
                            }
                            if (!data) {
                                return null;
                            }

                            let props = {};

                            if (query.hasOwnProperty('adapt')) {
                                data = query.adapt(data);
                            }
                            props[propName] = data;

                            return <Component {...this.props} {...props}/>
                        }}
                    </Query>
                );
            }
        }

        return WithQuery;
    };
};

export const withMutation = (propName, mutation) => {

    return (Component) => {

        class WithMutation extends React.Component {

            render() {
                let mutationProps = {};

                if (mutation.hasOwnProperty('refetch')) {
                    mutationProps.refetchQueries = mutation.refetch.map((api) => ({query: api.query}));
                }

                return (
                    <Mutation mutation={mutation.mutation} {...mutationProps}>
                        {(mutate) => {

                            let props = {};
                            props[propName] = (variables) => mutate({variables: variables}).then(
                                ({data}) => {

                                    if (mutation.hasOwnProperty('adapt')) {
                                        data = mutation.adapt(data);
                                    }
                                    return data;
                                },
                                (error) => adaptError(error)
                            );

                            return <Component {...this.props} {...props}/>
                        }}
                    </Mutation>
                );
            }
        }

        return WithMutation;
    };
};

