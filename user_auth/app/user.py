import bcrypt

from uuid import UUID
from dataclasses import dataclass
from typing import List
from typing import Optional

from graphql_api import field, GraphQLError
from sqlalchemy_gql.orm_base import ModelBase
from sqlalchemy_orm.filter import Filter


@dataclass
class UserFilter(Filter):
    """
    The `UserFilterInput` ObjectType represents a set of attributes for
    filtering an `User` search.

    Attributes:
        id: The uuid for the `User`.
        email: A case-insensitive filter on the email of the `User`.
        receive_emails: Find user who ticked the receive emails box.
    """
    id: UUID = None
    email: str = None
    receive_emails: bool = None

    def criterion(self, entities=None):
        criterion = super().criterion(entities=entities)

        if self.id:
            criterion.append(User.id == self.id)

        if self.email:
            criterion.append(User.email == self.email)

        if self.receive_emails:
            criterion.append(
                User.receive_emails == self.receive_emails
            )

        return criterion


class User(ModelBase):
    """
    The `User` ObjectType represents a email based user account.
    """
    name: Optional[str] = None
    email: str = None
    receive_emails: bool = False
    password_hash: bytes = None

    @classmethod
    def graphql_exclude_fields(cls) -> List[str]:
        """
        Don't expose the 'password_hash' to the GraphQL API
        """
        return ['password_hash']

    @classmethod
    def get(cls, id: UUID = None, email: str = None):
        if id:
            return super().get(id=id)

        if email:
            return cls.filter(email=email).one_or_none()

    @classmethod
    def exists(cls, email: str):
        return cls.filter(email=email).one_or_none() is not None

    def __init__(
        self,
        email,
        password,
        name=None,
        receive_emails=False
    ):

        if not email or "@" not in email or "." not in email:
            raise GraphQLError(
                f"Invalid Email",
                extensions={
                    "email": f"Invalid Email"
                }
            )

        if User.exists(email):
            raise GraphQLError(
                f"User with email {email} already exists",
                extensions={
                    "email": f"User with email {email} already exists"
                }
            )

        if not password or len(password) < 3:
            raise GraphQLError(
                f"Password must be 3 characters minimum",
                extensions={
                    "password": f"Password must be 3 characters minimum"
                }
            )

        super().__init__()
        self.email = email
        self.password = password
        self.name = name
        self.receive_emails = receive_emails

    @property
    def password(self):
        raise AttributeError(
            "The password is not stored directly, it can only be set"
        )

    @password.setter
    def password(self, plaintext_password):
        if plaintext_password:
            salt = bcrypt.gensalt()
            plaintext_password = plaintext_password.encode("utf-8")
            self.password_hash = bcrypt.hashpw(plaintext_password, salt)

    def check_password(self, plaintext_password: str) -> bool:
        """
        Check if a password is valid for the `User`.
        The password should be a plaintext password string.
        """
        return bcrypt.checkpw(
            plaintext_password.encode('utf-8'),
            self.password_hash
        )

    @field(mutable=True)
    def update(
        self,
        email: str = None,
        current_password: str = None,
        new_password: str = None,
        name: str = None,
        receive_emails: bool = None
    ) -> 'User':
        """
        Updates a `User`
        """

        if email:
            self.email = email

        if new_password:

            if not self.check_password(plaintext_password=current_password):
                raise GraphQLError(
                    f"Incorrect current password",
                    extensions={
                        "current_password": f"Incorrect current password"
                    }
                )

            self.password = new_password

        if name:
            self.name = name

        if receive_emails:
            self.receive_emails = receive_emails

        return self
