from typing import Optional, List
from graphql_api import field, GraphQLError
from sqlalchemy_orm.order_by import OrderBy, OrderByDirection

from app.user import User, UserFilter
from app.user_session import \
    USER_SESSION_STATE, \
    RequiredUserSessionState, \
    user_session, \
    clear_user_session_context, \
    set_user_session_context


class UserPortalAPI:

    @field({USER_SESSION_STATE: RequiredUserSessionState.ANY})
    def api_version(self) -> str:
        return "1.0.0"

    @field({USER_SESSION_STATE: RequiredUserSessionState.SIGNED_OUT}, mutable=True)
    def sign_up(
        self,
        name: str,
        email: str,
        password: str,
        receive_emails: bool = True
    ) -> User:
        """
        Register a new `User`
        """
        user = User(
            email=email,
            password=password,
            name=name,
            receive_emails=receive_emails
        )

        user.create()
        set_user_session_context(user)

        return user

    @field({USER_SESSION_STATE: RequiredUserSessionState.SIGNED_OUT}, mutable=True)
    def sign_in(self, email: str, password: str) -> User:
        """
        Sign in to an existing `User`
        """
        if not email:
            raise GraphQLError(
                "Invalid Email",
                extensions={"email": "Invalid Email"}
            )

        user = self.user(filter=UserFilter(email=email))

        if not user:
            raise GraphQLError(
                "User with email does not exist",
                extensions={"email": "User with email does not exist"}
            )

        if user.check_password(plaintext_password=password):
            set_user_session_context(user)
            return user
        else:
            raise GraphQLError(
                "Incorrect password",
                extensions={"password": "Incorrect password"}
            )

    @field(mutable=True)
    def sign_out(self) -> bool:
        """
        Sign out from the current `User`
        """
        clear_user_session_context()
        return True

    @field({USER_SESSION_STATE: RequiredUserSessionState.ANY})
    def is_signed_in(self) -> bool:
        """
        Check if the current `User` is signed in
        """
        return bool(user_session)

    @field({USER_SESSION_STATE: RequiredUserSessionState.SIGNED_IN})
    def me(self) -> User:
        """
        Get the current `User`
        """
        return user_session

    @field
    def user(self, filter: UserFilter) -> Optional[User]:
        """
        Finds a single `User` with a filter.
        """
        return User.query().filter(filter).one_or_none()

    @field
    def all_users(
        self,
        limit: int = 50,
        offset: int = 0,
        order_by: List[OrderBy] = None,
        filter: UserFilter = None
    ) -> List[User]:
        """
        Finds all `Users` with a filter.

        :param limit: The maximum number of `Users` that should be returned.
        :param offset: The offset, only return `Users`
        after the nth (offset) `User`.
        :param order_by:
        :param filter:
        :return:
        """
        if order_by is None:
            # noinspection PyArgumentList
            order_by = [OrderBy(key='name', direction=OrderByDirection.asc)]

        return User \
            .filter(filter) \
            .order_by(*order_by) \
            .offset(offset) \
            .limit(limit) \
            .all()
