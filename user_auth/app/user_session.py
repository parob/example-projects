import os
import enum
from typing import Optional, Callable

from context_helper.context import ctx, ContextNotAvailable, Context
from werkzeug.local import LocalProxy
from werkzeug.wrappers.request import Request
from werkzeug.wrappers.response import Response
from secure_cookie.cookie import SecureCookie

from app.user import User, UserFilter

USER_SESSION_STATE = "user_session_state"


class RequiredUserSessionState(enum.Enum):
    ANY = "any"
    SIGNED_IN = "signed_in"
    SIGNED_OUT = "signed_out"


def get_cookie_session(request: Request) -> SecureCookie:
    if hasattr(request, 'session'):
        return request.session
    else:
        request.session = SecureCookie.load_cookie(
            request,
            secret_key=os.environ.get('SESSION_SECRET_KEY', 'secret_key')
        )
        return request.session


def load_user_session(request: Request) -> Optional['User']:
    """
    Load a user from the request
    """
    user_id = get_cookie_session(request=request).get('user_id')

    if user_id:
        from main import UserPortalAPI
        return UserPortalAPI().user(filter=UserFilter(id=user_id))


def save_user_session(
    user: 'User',
    request: Request,
    response: Response
):
    """
    Save the user to the response
    """
    if not response:
        return

    cookie_session = get_cookie_session(request=request)

    if user and hasattr(user, 'id'):
        cookie_session['user_id'] = str(user.id)
        cookie_session.save_cookie(response)

    elif 'user_id' in cookie_session:
        del cookie_session['user_id']
        cookie_session.save_cookie(response)


def get_user_session_context(
    context_key_name="user_session"
) -> Optional['User']:
    """
    The current `User`
    """
    try:
        return getattr(ctx, context_key_name)
    except (ContextNotAvailable, AttributeError):
        return None


def set_user_session_context(
    user: Optional['User'],
    context_key_name="user_session"
):
    """
    Set the current `User`
    """
    setattr(ctx, context_key_name, user)


def clear_user_session_context(
    context_key_name="user_session"
):
    """
    Clear the current `User`
    """
    setattr(ctx, context_key_name, None)


def with_user_session(
    func: Callable = None,
    context_key_name="user_session"
):
    """
    Get the user_session and wrap `func` in a context with the user_session.
    """
    def with_context(request: Request):
        with Context(**{context_key_name: None}):
            user = load_user_session(request=request)
            set_user_session_context(user)

            try:
                response = func(request=request)
            except Exception:
                raise
            else:
                save_user_session(
                    user=get_user_session_context(),
                    request=request,
                    response=response
                )
            finally:
                clear_user_session_context()

            return response

    return with_context


def user_session_middleware(next_, context):
    """
    GraphQL middleware to validate a user_session state before the field is
     resolved, by default every field expects a user to be SIGNED_IN.
    """
    # ignore GraphiQL requests
    if context.request.info.field_name == '__typename':
        return next_()

    login_state = context.field.meta.get(
        USER_SESSION_STATE,
        RequiredUserSessionState.SIGNED_IN
    )

    field = context.request.info.field_name

    if login_state == RequiredUserSessionState.SIGNED_IN and not user_session:
        raise PermissionError(f"You must be logged in to access {field}.")

    elif login_state == RequiredUserSessionState.SIGNED_OUT and user_session:
        raise AttributeError(f"Logged in as '{user_session}', You must be "
                             f"logged out to access {field}.")

    return next_()


user_session: User = LocalProxy(get_user_session_context)
