import mimetypes
import os

from werkzeug import Response
from graphql_api import GraphQLAPI
from graphql_http_server import GraphQLHTTPServer
from sqlalchemy_gql.orm_base import DatabaseManager

from app.user_session import with_user_session, user_session_middleware
from app.api import UserPortalAPI


# check if app is deployed in a Google Cloud Function
is_google_cloud_function = os.getenv(
    "FUNCTION_SIGNATURE_TYPE", os.getenv("FUNCTION_TRIGGER_TYPE")) == "http"

if is_google_cloud_function:
    # use an in-memory database
    db_manager = DatabaseManager(url="sqlite:///:memory:")

    # setup Google Cloud Logging
    import google.cloud.logging
    client = google.cloud.logging.Client()
    client.get_default_handler()
    client.setup_logging()

else:
    db_manager = DatabaseManager()
    db_manager.install()

default_query = open(
    os.path.join(os.path.dirname(__file__), './example.graphql'), 'r'
).read()

server = GraphQLHTTPServer.from_api(
    api=GraphQLAPI(root=UserPortalAPI),
    middleware=[user_session_middleware],
    graphiql_default_query=default_query,
    allow_cors=True
)


@db_manager.with_db_session
@with_user_session
def main(request):

    # if in Google Cloud Functions and the db is empty, install the db models
    if is_google_cloud_function and db_manager.db.is_empty():
        db_manager.install()

    if request.path.lower() == "/schema":
        return Response(db_manager.db.entity_relationship_diagram(), content_type="text/html")

    # serve the static data frontend for the web app
    if request.path.lower() != "/graphql" and request.method.lower() == "get":
        static_data_path = os.path.join(os.path.dirname(__file__), "web_app/build/")
        request_path = request.path
        filename = os.path.join(static_data_path, request_path)

        while "/" in request_path and not os.path.isfile(filename):
            request_path = request_path.split("/", 1)[1]
            filename = os.path.join(static_data_path, request_path)

        if not os.path.isfile(filename):
            filename = os.path.join(static_data_path, "index.html")

        return Response(
            open(filename, 'rb').read(),
            content_type=mimetypes.guess_type(filename, False)[0]
        )

    return server.dispatch(request=request)


if __name__ == "__main__":
    server.run(main=main, port=3502)
