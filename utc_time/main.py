from graphql_api import GraphQLAPI, field
from graphql_http_server import GraphQLHTTPServer
from datetime import datetime

class UTCTime:

    @field
    def now(self) -> str:
        return str(datetime.now())


server = GraphQLHTTPServer.from_api(api=GraphQLAPI(root=UTCTime))

# define main fuction for Google Cloud Function support
def main(request):
    return server.dispatch(request=request)

if __name__ == "__main__":
    server.run(main=main, port=3511)
