from dataclasses import dataclass
from typing import List
from uuid import UUID

from graphql_api import GraphQLAPI, field
from graphql_api.error import GraphQLError
from graphql_api.remote import GraphQLRemoteExecutor, GraphQLRemoteObject
from graphql_http_server import GraphQLHTTPServer

@dataclass
class PokemonDimensionStub:
    minimum: str
    maximum: str

    @property
    def average(self):
        minimum = float(self.minimum.replace("kg", "").replace("m", ""))
        maximum = float(self.maximum.replace("kg", "").replace("m", ""))
        return (minimum + maximum) / 2.0


@dataclass
class PokemonStub:
    id: UUID
    number: str
    name: str
    weight: PokemonDimensionStub
    height: PokemonDimensionStub
    types: List[str]


@dataclass
class PokemonAPIStub:

    @field
    def pokemon(self, id: str, name: str) -> PokemonStub:
        pass


# noinspection PyTypeChecker
pokemon_api: PokemonAPIStub = GraphQLRemoteObject(
    executor=GraphQLRemoteExecutor(
        name="PokemonAPI",
        url="https://graphql-pokemon2.vercel.app/",
        http_method="POST"
    ),
    python_type=PokemonAPIStub
)


class PokemonBMI:

    @field
    def bmi(self, name: str) -> float:
        pokemon = pokemon_api.pokemon(name=name)

        if not pokemon.height:
            raise GraphQLError(f"Pokemon '{name}' does not exist.")

        average_height = pokemon.height.average
        average_weight = pokemon.weight.average

        bmi = average_weight / (average_height ** 2)

        return round(bmi, 1)


default_query = """# Pokemon BMI calculator written in Python with ObjectQL

query {
    pikachu_bmi: bmi(name: "Pikachu")
    snorlax_bmi: bmi(name: "Snorlax")
    dragonite_bmi: bmi(name: "Dragonite")
    gastly_bmi: bmi(name: "Gastly")
}
"""

server = GraphQLHTTPServer.from_api(
    api=GraphQLAPI(root=PokemonBMI),
    graphiql_default_query=default_query
)


# define main function for Google Cloud Function support
def main(request):
    return server.dispatch(request=request)


if __name__ == "__main__":
    server.run(main=main, port=3507)

